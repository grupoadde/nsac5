<?php

namespace App\Helpers;

use Illuminate\Support\Facades\Auth;
use JeroenNoten\LaravelAdminLte\Menu\Builder;
use JeroenNoten\LaravelAdminLte\Menu\Filters\FilterInterface;

class FiltrarMenu implements FilterInterface
{
    public function transform($item, Builder $builder)
    {
        if (isset($item['role'])) {
            if (is_array($item['role'])) {
                if (! Auth::user()->hasAllRoles($item['role'])) {   return false;   }
            } else {
                if (! Auth::user()->hasRole($item['role'])) {
                    return false;
                }
            }
        }

        if (isset($item['anyrole'])) {
            if (is_array($item['anyrole'])) {
                if (! Auth::user()->hasAnyRoles($item['role'])) {   return false;   }
            } else {
                if (! Auth::user()->hasRole($item['role'])) {
                    return false;
                }
            }
        }

        if (isset($item['header'])) {
            $item = $item['header'];
        }

        return $item;
    }
}