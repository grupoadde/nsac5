<?php

namespace App\Http\Controllers\Admin\FichasAvaliacao;

use App\Http\Controllers\Controller;
use App\Models\FichaAvaliacaoGrupo;
use App\Models\FichaAvaliacaoItem;
use Illuminate\Http\Request;

class FichasGrupoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $fichasgrupo = FichaAvaliacaoGrupo::all(); //Get all permissions
        return view('ficha-avaliacao.grupos.index')->with(['fichasgrupo'=> $fichasgrupo]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        return view('ficha-avaliacao.grupos.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if(count($request->all())>0)
        {
            $grupo=new FichaAvaliacaoGrupo;
            $grupo->descricao=$request->descricao;
            $grupo->ativo=TRUE;
            $grupo->save();
        }
        return redirect()->route('fichagrupos.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $grupo=FichaAvaliacaoGrupo::find($id);
        $itens=FichaAvaliacaoItem::where('grupo',$id)->orderBy('id')->get();
        return view('ficha-avaliacao.grupos.edit')->with(['grupo'=>$grupo,'itens'=>$itens]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $grupo=FichaAvaliacaoGrupo::find($id);
        $grupo->descricao=$request->descricao;
        $grupo->save();

       return redirect()->route('fichagrupos.index');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $grupo=FichaAvaliacaoGrupo::find($id);

        if($grupo->ativo==TRUE)
            $grupo->ativo=FALSE;
        else
            $grupo->ativo=TRUE;

        $grupo->save();
        return redirect()->route('fichagrupos.index');
    }
}
