<?php

namespace App\Http\Controllers\Admin\FichasAvaliacao;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\FichaAvaliacaoItem;
use App\Models\FichaAvaliacaoGrupo;

class FichasItemController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($id)
    {
        //
        $grupo=FichaAvaliacaoGrupo::find($id);
        return view('ficha-avaliacao.itens.create',['grupo'=>$grupo]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if(count($request->all())>0)
        {
            $item=new FichaAvaliacaoItem;
            $item->descricao=$request->descricao;
            $item->tem_texto=(integer)$request->tem_texto;
            $item->grupo=$request->codigo_grupo;
            $item->ativo=TRUE;
            $item->save();
        }
        return redirect()->route('fichagrupos.edit',['id'=>$request->codigo_grupo]);

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $item=FichaAvaliacaoItem::find($id);
        return view('ficha-avaliacao.itens.edit')->with(['item'=>$item]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $item=FichaAvaliacaoItem::find($id);
        $item->descricao=$request->descricao;
        $item->tem_texto=(integer)$request->tem_texto;
        $item->save();
        return redirect()->route('fichagrupos.edit',['id'=>$item->grupo]);

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {

        $item=FichaAvaliacaoItem::find($id);

        if($item->PossuiAlunos) {

            if ($item->ativo == TRUE)
                $item->ativo = FALSE;
            else
                $item->ativo = TRUE;

            $item->save();
        } else {
            $item->delete();
        }
        return redirect()->route('fichagrupos.edit',['id'=> $item->grupo]);
    }

}