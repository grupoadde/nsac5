<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Professor;
use Illuminate\Http\Request;
use App\Http\Controllers\Auth;
use Illuminate\Support\Facades\Session;

class ProfessorController extends Controller
{

    public function __construct() {
        $this->middleware(['auth', 'clearance'])->except('index', 'show');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $professores = Professor::orderby('nome', 'asc')->paginate(10); //show only 5 items at a time in descending order

        return view('admin.professores.index', compact('professores'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.professores.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'nome'=>'required|max:80',
            'email'=>'nullable|email',
        ]);

        $nome = $request['nome'];
        $rg = $request['rg'];
        $funcao = $request['funcao'];
        $email = $request['email'];
        $situacao= $request['situacao'];
        $apelido = $request['apelido'];
        $carga_horaria = $request['carga_horaria'];
        $sexo = $request['sexo'];
        $cpf = $request['cpf'];
        $rd = $request['rd'];

        $professor = Professor::create($request->only('nome'));

        //Display a successful message upon save
        return redirect()->route('professores.index')
            ->with('flash_message', 'Professor,
             '. $professor->nome.' cadastrado');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $professor = Professor::findOrFail($id); //Find professor of id = $id

        $atribuicoes = $professor->atribuicoes()->where('data_inicial', '<=',date('Y-m-d'))->where('data_final','>=',date('Y-m-d'))->get();

        return view ('admin.professores.show', compact('professor', 'atribuicoes'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $professor = Professor::findOrFail($id); //Find professor of id = $id

        return view('admin.professores.edit', compact('professor'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'nome'=>'required|max:80',
        ]);

        $professor = Post::findOrFail($id);
        $professor->nome = $request->input('nome');
        $professor->rg = $request->input('rg');
        $professor->funcao = $request->input('funcao');
        $professor->email = $request->input('email');
        $professor->situacao= $request->input('situacao');
        $professor->apelido = $request->input('apelido');
        $professor->carga_horaria = $request->input('carga_horaria');
        $professor->sexo = $request->input('sexo');
        $professor->cpf = $request->input('cpf');
        $professor->rd = $request->input('rd');

        $professor->save();

        return redirect()->route('professores.show',
            $professor->codigo)->with('flash_message',
            'Professor '. $professor->nome.' atualizado');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $professor = Professor::findOrFail($id);
        $professor->delete();

        return redirect()->route('professores.index')
            ->with('flash_message',
                'Professor apagado com sucesso.');
    }
}
