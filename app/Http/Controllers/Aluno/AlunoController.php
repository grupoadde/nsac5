<?php

namespace App\Http\Controllers\Aluno;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\User;
use App\Models\Cidade;
use Illuminate\Support\Facades\Auth;

class AlunoController extends Controller {

    public function perfil() {
        $perfil = Auth::user()->dado;
        $cidades = Cidade::get()->where('estado', '=', $perfil->getEndereco->getCidade->estado);
        //dd(Auth::user()->dado->getEndereco->getCidade);
        return view('aluno.perfil')->with('perfil', $perfil)->with('cidades', $cidades);
    }

}