<?php

namespace App\Http\Controllers\Professor;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Professor;
use Illuminate\Support\Facades\Auth;

class ConfigController extends Controller
{
    public function index()
    {
        $prof = Professor::where('email', Auth::user()->email)->first();
        return view('professor.perfil.config', ['prof' => $prof]);
    }


}
