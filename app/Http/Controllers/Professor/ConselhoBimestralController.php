<?php

namespace App\Http\Controllers\Professor;

use App\Helpers\Sistema;
use App\Models\Dado;
use App\Models\Matricula;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Nota;
use App\Models\Disciplina;
use App\Models\Turma;

class ConselhoBimestralController extends Controller
{
    public function index()
    {
        $dados=[
            'alunos'=>'',
            'turma'=>'',
            'nota_minima'=>6,
            'falta'=>25,
            'bimestre'=>Sistema::BimestreAtual()->bimestre,
        ];

        return view('coordenador.conselhobimestral.index',['dados' => $dados]);
    }

    private function alunoscomNotasAbaixoDe($alunos,$nota_minima,$bimestre,$turma,$falta_minima)
    {
        $recuperacoes=null; $disciplinas_recuperacoes=null; $fb='fb'.$bimestre;
		$i=0;$aux_aluno=0;$flag=0;//Evitar que o aluno seja inserido mais de uma vez na lista de alunos em recuperação
		
		if($turma->turma_integrada==null)
			$turma_medio=0;
		else
			$turma_medio=$turma->turma_integrada;
	
        foreach ($alunos as $aluno)
        {
			$flag=0;
			$disciplinas=$aluno->notas_ano_atual($turma->codigo,$turma_medio);
		
			foreach($disciplinas as $item=>$value)
			{
				if(count($value->notas)>0)
				{
					if($aluno->dado->notaabaixode($value->id,$nota_minima*10,$bimestre)==TRUE||$value->notas[0]->$fb>$falta_minima)
					{
						$disciplinas_recuperacoes[$aux_aluno][$item]=$value;
						if($flag==0)
						{
							$recuperacoes[$i]=$aluno; 
							$i++;
							$flag=1;
						}
					}
				}
			}
			$aux_aluno=$i;
        }	
		return compact('recuperacoes','disciplinas_recuperacoes');
    }


    public function show(Request $request)
    {
        $info=$request->all();

		if(ISSET($info['aux']))
            $aux=$info['aux'];
        else
            $aux=0;

		$turma=Turma::find($info['turma']);
		$alunos=Matricula::where('turma',$turma->codigo)->with('dado')->get()->sortBy('dado.nome');
		$recuperacoes_total=$this->alunoscomNotasAbaixoDe($alunos,$info['nota_minima'],$info['bimestre_atual'],$turma,$info['falta_minima']);
		$recuperacoes=$recuperacoes_total['recuperacoes'];
		$disciplinas_recuperacoes=$recuperacoes_total['disciplinas_recuperacoes'];

		$dados=[
					'alunos'=>$alunos,
					'turma'=>$turma,
					'nota_minima'=>$info['nota_minima'],
					'falta'=>$info['falta_minima'],
					'bimestre'=>$info['bimestre_atual']
				];
		//dump(Sistema::nome_professor(992,193));
		return view('coordenador.conselhobimestral.index',compact('dados','aux','recuperacoes','disciplinas_recuperacoes'));
	}


}
