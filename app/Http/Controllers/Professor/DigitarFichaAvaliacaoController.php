<?php

namespace App\Http\Controllers\Professor;

use App\Models\Bimestre;
use App\Models\Dado;
use App\Models\Disciplina;
use App\Models\FichaAvaliacaoAluno;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\FichaAvaliacaoGrupo;
use App\Models\FichaAvaliacaoItem;
use App\Models\Nota;

class DigitarFichaAvaliacaoController extends Controller
{

    private function CriarSeNaoExiste($aluno, $disciplina) {
        $nota = Nota::where('aluno', $aluno)->where('disciplina', $disciplina)->first();
        if (count($nota) == 0) {
            $nota = new Nota;
            $nota->aluno = $aluno;
            $nota->disciplina = $disciplina;
            $nota->save();
        }
        return $nota;
    }

    private function ApagarSeExiste($aluno, $disciplina) {
        $nota = Nota::where('aluno', $aluno)->where('disciplina', $disciplina)->first();
        if (count($nota) == 1) {
            $nota->delete();
        }
    }

    public function buscarAlunoseNotas($disciplina)
    {

        $alunos_dependentes = Disciplina::find($disciplina)->alunos_dependentes();
        foreach ($alunos_dependentes as $aluno) {
            $this->CriarSeNaoExiste($aluno->matricula, $disciplina);
        }

        $alunos_adaptacao = Disciplina::find($disciplina)->alunos_adaptacao();
        foreach ($alunos_adaptacao as $aluno) {
            $this->CriarSeNaoExiste($aluno->matricula, $disciplina);
        }

        $alunos_dispensados = Disciplina::find($disciplina)->alunos_dispensados();
        foreach ($alunos_dispensados as $aluno) {
            $this->ApagarSeExiste($aluno->matricula, $disciplina);
        }

        $alunos_matriculados = Disciplina::find($disciplina)->alunos_matriculados();
        foreach ($alunos_matriculados as $matriculas) {
            $this->CriarSeNaoExiste($matriculas->aluno,$disciplina);
        }

        $disciplina = Disciplina::find($disciplina);

        return compact('alunos_dispensados', 'alunos_adaptacao', 'alunos_dependentes', 'alunos_matriculados', 'disciplina');

    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($disciplina,$bimestre)
    {
        //
        $dados = $this->buscarAlunoseNotas($disciplina);
        return view('professor.ficha-avaliacao.aluno.index',['dados'=>$dados,'bimestre'=>$bimestre]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($disciplina,$bimestre,$aluno)
    {
        $id_disciplina=Disciplina::find($disciplina);
        $fichagrupos=FichaAvaliacaoGrupo::where('ativo',true)->get();
        $estudante=Dado::where('matricula',$aluno)->first();

        return view('professor.ficha-avaliacao.aluno.digitar',['fichagrupos'=>$fichagrupos,'disciplina'=>$id_disciplina, 'aluno'=>$estudante, 'bimestre'=>$bimestre]);

    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $itens=$request->all();
        $disciplina=$request->disciplina;
        $aluno=$request->aluno;
        $bimestre_atual=$request->bimestre;

        FichaAvaliacaoAluno::where('matricula', $aluno)->where('disciplina', $disciplina)->where('bim', $bimestre_atual)->delete();

        if (isset($itens['item'])) {
            foreach ($itens['item'] as $item => $value)
            {
                $fichaavaliacao=new FichaAvaliacaoAluno;
                $fichaavaliacao->matricula=$aluno;
                $fichaavaliacao->disciplina=$disciplina;
                $fichaavaliacao->cod_item=$item;
                $fichaavaliacao->bim=$bimestre_atual;

                if(isset($itens['obs'][$item]) == true)
                    $fichaavaliacao->obs=$itens['obs'][$item];

                $fichaavaliacao->save();
            }
        }
        return redirect()->route('professor.fichaavaliacao.index',['disciplina'=>$disciplina,'bimestre'=>$bimestre_atual]);
    }

//    /**
//     * Display the specified resource.
//     *
//     * @param  \App\Models\FichaAvaliacaoAluno\DigitarFichaAvaliacao  $digitarFichaAvaliacao
//     * @return \Illuminate\Http\Response
//     */
    public function show()
    {

    }

//    /**
//     * Show the form for editing the specified resource.
//     *
//     * @param  \App\DigitarFichaAvaliacao  $digitarFichaAvaliacao
//     * @return \Illuminate\Http\Response
//     */
    public function edit(FichaAvaliacaoAluno $digitarFichaAvaliacao)
    {
        //
    }

//    /**
//     * Update the specified resource in storage.
//     *
//     * @param  \Illuminate\Http\Request  $request
//     * @param  \App\DigitarFichaAvaliacao  $digitarFichaAvaliacao
//     * @return \Illuminate\Http\Response
//     */
    public function update(Request $request, FichaAvaliacaoAluno $digitarFichaAvaliacao)
    {
        //
    }

//    /**
//     * Remove the specified resource from storage.
//     *
//     * @param  \App\DigitarFichaAvaliacao  $digitarFichaAvaliacao
//     * @return \Illuminate\Http\Response
//     */
    public function destroy(FichaAvaliacaoAluno $digitarFichaAvaliacao)
    {
        //
    }
}
