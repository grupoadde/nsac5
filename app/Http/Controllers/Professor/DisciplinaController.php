<?php

namespace App\Http\Controllers\Professor;

use App\Helpers\Sistema;
use App\Http\Controllers\Controller;
use App\Models\Atribuicao;
use App\Models\Professor;
use App\Models\Disciplina;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Config;
use App\Models\Turma;
use Barryvdh\DomPDF\Facade as PDF;
use Illuminate\Http\Request;






class DisciplinaController extends Controller
{
    public function __construct()
    {
        $this->middleware(['auth', 'professor']);
    }

    public function index()
    {
        $prof = Professor::where('email', Auth::user()->email)->first();
        $atribuicao = Atribuicao::where('professor', $prof->codigo)->where('data_inicial', '<=', date('Y-m-d'))->where('data_final', '>=', date('Y-m-d'))->with('disciplinas.turmas')->get();
        return view('professor.disciplinas.index', compact('atribuicao','bimestre_atual'));
    }

    public function edit($id)
    {
        return view('disciplinas.edit');
    }

    public function show($disciplina)
    {
        $bimestre=Sistema::BimestreAtual();
        $disciplina=Disciplina::find($disciplina);
        $turma = Turma::find($disciplina->turma);

        return view('professor.disciplinas.show', compact('disciplina','bimestre', 'turma'));
    }

    public function lista($id, Request $request)
    {
        $titulo = $request->titulo;

        $disciplina = Disciplina::find($id);

        $alunos_dependentes = $disciplina->alunos_dependentes();
        $alunos_adaptacao = $disciplina->alunos_adaptacao();
        $alunos_dispensados = $disciplina->alunos_dispensados();
        $alunos_matriculados = $disciplina->alunos_matriculados();

        $dados = compact('alunos_dispensados', 'alunos_adaptacao', 'alunos_dependentes', 'alunos_matriculados', 'disciplina', 'titulo');



        // $turma = Turma::find($disciplina->turma);
        $pdf = PDF::loadView('professor.disciplinas.lista', compact('dados'));
      //  $pdf->setBasePath(public_path());

        //$pdf->setOptions('chroot', public_path());

        return $pdf->stream($dados['disciplina']->CodigoCti.'_'.$dados['disciplina']->abreviacao.'_'.date('Y').'.pdf');
    }


}
