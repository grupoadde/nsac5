<?php

namespace App\Http\Controllers\Professor;

use App\Helpers\Sistema;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Bimestre;
use App\Models\Disciplina;
use App\Models\Nota;
use Barryvdh\DomPDF\Facade as PDF;



class NotaController extends Controller
{

    public function __construct()
    {
        $this->middleware(['auth', 'professor']);
    }

    private function CriarSeNaoExiste($aluno, $disciplina) {
        $nota = Nota::where('aluno', $aluno)->where('disciplina', $disciplina)->first();
        if (count($nota) == 0) {
            $nota = new Nota;
            $nota->aluno = $aluno;
            $nota->disciplina = $disciplina;
            $nota->save();
        }
        return $nota;
    }

    private function ApagarSeExiste($aluno, $disciplina) {
        $nota = Nota::where('aluno', $aluno)->where('disciplina', $disciplina)->first();
        if (count($nota) == 1) {
            $nota->delete();
        }
    }

    public function buscarAlunoseNotas($disciplina, $bimestre)
    {

        $disciplina = Disciplina::find($disciplina);

        $alunos_dependentes = $disciplina->alunos_dependentes();
        foreach ($alunos_dependentes as $aluno) {
            $this->CriarSeNaoExiste($aluno->matricula, $disciplina->id);
        }

        $alunos_adaptacao = $disciplina->alunos_adaptacao();
        foreach ($alunos_adaptacao as $aluno) {
            $this->CriarSeNaoExiste($aluno->matricula, $disciplina->id);
        }

        $alunos_dispensados = $disciplina->alunos_dispensados();
        foreach ($alunos_dispensados as $aluno) {
            $this->ApagarSeExiste($aluno->matricula, $disciplina->id);
        }

        $alunos_matriculados = $disciplina->alunos_matriculados();
        foreach ($alunos_matriculados as $matriculas) {
            $this->CriarSeNaoExiste($matriculas->aluno, $disciplina->id);
        }

//        $notas = Nota::where('disciplina', $disciplina->id)->get();
//
//        $bimestre;

        return compact('alunos_dispensados', 'alunos_adaptacao', 'alunos_dependentes', 'alunos_matriculados', 'disciplina', 'bimestre');

    }

    public function index($disciplina)
    {
        return redirect()->route('professor.notas.notas_bimestre_atual',['disciplina'=>$disciplina,'bimestre'=>$this->bimestre_atual]);
    }

    public function edit($id, $bimestre)
    {
        $dados=$this->buscarAlunoseNotas($id, $bimestre);
        return view('professor.notas.index',['dados'=>$dados]);
    }

    public function update(Request $request)
    {
        //$bimestre=Bimestre::getBimestreAtual()->bimestre;

        $disciplina = $request->disciplina;
        $bimestre = $request->bimestre;

        //TESTAR SE BIMESTRE ESTÁ LIBERADO
        $notas = (array) $request->all();

        array_splice($notas,0,3);

        foreach ($notas as $item => $value) {

            if (strlen($item) == 9) {
                $aluno = substr($item,2,7);

                //PEGA A NOTA NO BANCO
                $nota = $this->CriarSeNaoExiste($aluno, $disciplina);

                if (substr($item,0,2) == 'nb') {
                    if($bimestre == 1) { $nota->nb1 = $value*10; }
                    if($bimestre == 2) { $nota->nb2 = $value*10; }
                    if($bimestre == 3) { $nota->nb3 = $value*10; }
                    if($bimestre == 4) { $nota->nb4 = $value*10; }
                }
                if (substr($item,0,2) == 'fb') {
                    if($bimestre == 1) { $nota->fb1 = $value; }
                    if($bimestre == 2) { $nota->fb2 = $value; }
                    if($bimestre == 3) { $nota->fb3 = $value; }
                    if($bimestre == 4) { $nota->fb4 = $value; }
                }
                if (substr($item,0,2) == 'nc') {
                    if($bimestre == 1) { $nota->nc1 = $value; }
                    if($bimestre == 2) { $nota->nc2 = $value; }
                    if($bimestre == 3) { $nota->nc3 = $value; }
                    if($bimestre == 4) { $nota->nc4 = $value; }
                }
                if (substr($item,0,2) == 'rc') {
                    if($bimestre == 5) { $nota->rec1 = $value; }
                    if($bimestre == 6) { $nota->rec2 = $value; }
                    if($bimestre == 7) { $nota->recfinal = $value; }
                }

                $nota->save();
            }
        }

        flash()->overlay('Notas Salvas com sucesso!', 'Nsac Online');

        if ($bimestre > 4) {
            return redirect()->route('professor.recuperacao.edit', [$disciplina, $bimestre-4]);
        } else {
            return redirect()->route('professor.notas.edit', [$disciplina, $bimestre]);
        }

    }

    public function show($id)
    {
        $bimestre = Sistema::BimestreAtual()->bimestre;
        $dados=$this->buscarAlunoseNotas($id,$bimestre);
        return view('professor.notas.index',['dados'=>$dados]);
    }



    public function boletim($id, Request $request)
    {
        $bimestre = Sistema::BimestreAtual()->bimestre;
        $dados=$this->buscarAlunoseNotas($id,$bimestre);
        $pdf = PDF::loadView('professor.disciplinas.boletim', compact('dados'));
        return $pdf->stream('boletim.pdf');

    }

}

