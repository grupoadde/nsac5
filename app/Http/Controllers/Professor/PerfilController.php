<?php

namespace App\Http\Controllers\Professor;

use App\Models\Professor_Telefone;
use App\Models\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Professor;
use Illuminate\Support\Facades\Auth;
use App\Models\Atribuicao;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Route;



class PerfilController extends Controller
{

    public function __construct()
    {
        $this->middleware(['auth', 'professor']);
    }

    public function index()
    {
        $prof = Professor::where('email', Auth::user()->email)->first();
        $atribuicao = Atribuicao::where('professor', $prof->codigo)->where('data_inicial', '<=', date('Y-m-d'))->where('data_final', '>=', date('Y-m-d'))->with('disciplinas.turmas')->get();
        return view('professor.perfil.index', compact('prof', 'atribuicao'));
    }

    public function update_dados(Request $request)
    {
        $professor = Professor::where('email', Auth::user()->email)->first();
        $user = User::where('email', $professor->email)->first();

        $professor_telefone = Professor_Telefone::where('dono',$professor->codigo)->first();


        $professor->update($request->all());
        $user->email = $request->email;
		$user->name = $request->apelido;

        $user->update();

        if($professor_telefone == null)
        {
            $professor_telefone = new Professor_Telefone();
            $professor_telefone->numero = $request->telefone;
            $professor_telefone->dono = $professor->codigo;
            $professor_telefone->save();
        }

        else
        {
            $professor_telefone->numero = $request->telefone;
            $professor_telefone->update();
        }




        return redirect()->route('professor.perfil')->with('modalalert', true)->with('mensagem','Dados atualizados com sucesso.');
    }

    public function update_login(Request $request)
    {
        $professor = Professor::where('email', Auth::user()->email)->first();
        $user = User::where('email', $professor->email)->first();

        if(Hash::check($request->atual,$user->password))
        {
            if($request->nova == $request->confirmacao)
            {
                $user->password = $request->nova;
                $user->save();
            }

            else
            {
                return redirect()->route('professor.perfil.edit.login')->with('modalalert', true)->with('mensagem','Senhas não coincidem. Digite novamente.');
            }

        }
        else
        {
            return redirect()->route('professor.perfil.edit.login')->with('modalalert', true)->with('mensagem','Senha atual incorreta. Digite novamente.');

        }

        return redirect()->route('professor.perfil')->with('modalalert', true)->with('mensagem','Os dados foram alterados com sucesso.');
    }
}

