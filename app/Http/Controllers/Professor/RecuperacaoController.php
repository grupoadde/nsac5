<?php

namespace App\Http\Controllers\Professor;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Disciplina;
use App\Models\Nota;

class RecuperacaoController extends Controller
{

    private function CriarSeNaoExiste($aluno, $disciplina) {
        $nota = Nota::where('aluno', $aluno)->where('disciplina', $disciplina)->first();
        if (count($nota) == 0) {
            $nota = new Nota;
            $nota->aluno = $aluno;
            $nota->disciplina = $disciplina;
            $nota->save();
        }
        return $nota;
    }

    private function ApagarSeExiste($aluno, $disciplina) {
        $nota = Nota::where('aluno', $aluno)->where('disciplina', $disciplina)->first();
        if (count($nota) == 1) {
            $nota->delete();
        }
    }

    public function buscarAlunoseNotas($disciplina, $recuperacao)
    {

        $alunos_dependentes = Disciplina::find($disciplina)->alunos_dependentes();
        foreach ($alunos_dependentes as $aluno) {
            $this->CriarSeNaoExiste($aluno->matricula, $disciplina);
        }

        $alunos_adaptacao = Disciplina::find($disciplina)->alunos_adaptacao();
        foreach ($alunos_adaptacao as $aluno) {
            $this->CriarSeNaoExiste($aluno->matricula, $disciplina);
        }

        $alunos_dispensados = Disciplina::find($disciplina)->alunos_dispensados();
        foreach ($alunos_dispensados as $aluno) {
            $this->ApagarSeExiste($aluno->matricula, $disciplina);
        }

        $alunos_matriculados = Disciplina::find($disciplina)->alunos_matriculados();
        foreach ($alunos_matriculados as $matriculas) {
            $this->CriarSeNaoExiste($matriculas->aluno,$disciplina);
        }

        $disciplina = Disciplina::find($disciplina);

        return compact('alunos_dispensados', 'alunos_adaptacao', 'alunos_dependentes', 'alunos_matriculados', 'disciplina', 'recuperacao');

    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($disciplina, $recuperacao)
    {

        $dados = $this->buscarAlunoseNotas($disciplina, $recuperacao);
/*        dump($dados);
        exit;*/
        return view('professor.recuperacao.digitar',['dados'=>$dados]);

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

}
