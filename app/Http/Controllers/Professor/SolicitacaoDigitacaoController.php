<?php

namespace App\Http\Controllers\Professor;

use App\Models\Disciplina_Autorizacao;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\Controller;
use App\Models\Professor;
use App\Models\Atribuicao;

class SolicitacaoDigitacaoController extends Controller
{
    //
    private function verificar_situacao_encerrada($solicitacoes)
    {
        foreach ($solicitacoes as $solicitacao) {
            if ($solicitacao->data_limite != null) {
                if ($solicitacao->data_limite < date('Y-m-d'))
                    $solicitacao->situacao = 4;
            } else
                $solicitacao->situacao = 1;


            $solicitacao->save();
        }
    }


    public function index()
    {
        $solicitacoes_digitacao=Disciplina_Autorizacao::with('disciplinas')->where('user_id',Auth::user()->id)->orderBy('data_limite','desc')->get();
        $this->verificar_situacao_encerrada($solicitacoes_digitacao);
        return view('professor.solicitacaodigitacao.index', compact('solicitacoes_digitacao'));
    }

    public function show(Request $request)
    {
        $dados=$request->all();
        $solicitacao=Disciplina_Autorizacao::find($dados['solicitacao']);
        $solicitacao->situacao=3;
        $solicitacao->save();

        if(ISSET($dados['recuperacao']))
        {
            return redirect()->route($dados['rota'],array('disciplina'=>$dados['disciplina'],'recuperacao'=>$dados['recuperacao']));
        }
        else
        {
            return redirect()->route($dados['rota'],array('disciplina'=>$dados['disciplina'],'bimestre'=>$dados['bimestre']));
        }
    }

    public function create()
    {
        $prof = Professor::where('email', Auth::user()->email)->first();
        $atribuicao = Atribuicao::where('professor', $prof->codigo)->where('data_inicial', '<=', date('Y-m-d'))->where('data_final', '>=', date('Y-m-d'))->with('disciplinas')->first();
        return view('professor.solicitacaodigitacao.create',['disciplinas'=>$atribuicao]);
    }

    public function store(Request $request)
    {
        $dados=$request->all();

        $solicitacao=new Disciplina_Autorizacao;
        $solicitacao->disciplina=$dados['disciplina'];
        $solicitacao->observacao=$dados['justificativa'];
        $solicitacao->tipo=$dados['tipo'];
        $solicitacao->situacao=1;
        $solicitacao->user_id=Auth::user()->id;
        $solicitacao->save();

        return redirect()->route('professor.solicitacaodigitacao.index');
    }

    public function destroy($id)
    {
        $solicitacao=Disciplina_Autorizacao::find($id);
        if($solicitacao->situacao==3)
        {
            return redirect()->route('professor.solicitacaodigitacao.index');
        }
        else
        {
            $solicitacao->delete();
        }
        return redirect()->route('professor.solicitacaodigitacao.index');
    }
}
