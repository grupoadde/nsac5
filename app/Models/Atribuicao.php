<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Atribuicao extends Model
{
    protected $table = 'atribuicao';

    protected $primaryKey = 'codigo';

    public $timestamps = false;

    public function professores()
    {
        return $this->belongsTo(Professor::class, 'codigo', 'professor');
    }

    public function disciplinas()
    {
        return $this->belongsToMany(Disciplina::class, 'disciplinas_atribuicao','atribuicao','disciplina')->withPivot('disciplina');
    }
}
