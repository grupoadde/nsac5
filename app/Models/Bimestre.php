<?php

namespace App\Models;

use App\Models\Functions\BimestreFunctions;
use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

class Bimestre extends Model
{
    protected $table='calendario.bimestres';
    protected $primaryKey='id';

    use BimestreFunctions;

    public function scopeBimestreAtual($query) {
        $query
            ->where('data_inicio','<=', Carbon::now() )
            ->where('data_fim','>=', Carbon::now() );
    }

   /*public static function getBimestreAtual()
    {
        $data_atual=date('Y-m-d');
        $bimestre_atual=Bimestre::select('bimestre')->where('data_inicio','<',$data_atual)->orWhere('data_inicio',$data_atual)->orderBy('id','desc')->first();
        return $bimestre_atual;
    }*/
}
