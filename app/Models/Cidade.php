<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\Endereco;

class Cidade extends Model
{
    protected $connection = "public";

    public function endereco() {
        return $this->belongsToMany(Endereco::class, 'codigo', 'codigo');
    }
}
