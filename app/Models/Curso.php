<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Curso extends Model
{
    protected $table = 'cursos';
    protected $primaryKey = 'codigo';

    const Tipos = [
        0 => 'Técnico',
        1 => 'Ensino Médio',
        2 => 'Integrado', //PARA CURSOS ANTIGOS
    ];
}
