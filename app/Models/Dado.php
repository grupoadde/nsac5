<?php

namespace App\Models;

use App\Models\Functions\DadoFunctions;
use App\Models\Relations\DadoRelations;
use Illuminate\Database\Eloquent\Model;

class Dado extends Model
{

    use DadoRelations;
    use DadoFunctions;

    protected $connection = "alunos";

    

    protected $table='alunos.dados';

    protected $primaryKey='matricula';

    public function getsexoFAttribute() {
        return $this->sexo == 0 ? 'o' : 'a';
    }

    public function getConselhoAttribute() {

    }

    public function getIdadeFormatadaAttribute()
    {
        $idade=date('Y')-date('Y',strtotime($this->data_de_nascimento));
        if(date('m')<date('m',strtotime($this->data_de_nascimento)))
            $idade--;
        if(date('m')==date('m',strtotime($this->data_de_nascimento))&&date('d')<date('d',strtotime($this->data_de_nascimento)))
            $idade--;
        return $idade;
    }



}
