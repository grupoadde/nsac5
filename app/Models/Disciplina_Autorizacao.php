<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Disciplina_Autorizacao extends Model
{
    protected $table = 'disciplinas_autorizacoes';
    public $timestamps=false;

    const TIPO=
        [
            '1'=>' Nota - 1º Bimestre',
            '2'=>' Nota - 2º Bimestre',
            '3'=>' Nota - 3º Bimestre',
            '4'=>' Nota - 4º Bimestre',
            '5'=>' Recuperação - 1º Semestre',
            '6'=>' Recuperação - 2º Semestre',
            '7'=>' Recuperação Final',
            '8'=>' Ficha de Avaliação - 1º Bimestre',
            '9'=>' Ficha de Avaliação - 2º Bimestre',
            '10'=>'Ficha de Avaliação - 3º Bimestre',
            '11'=>'Ficha de Avaliação - 4º Bimestre',
        ];

    const SITUACAO=
        [
            '1'=> 'Aguardando Aprovação',
            '2'=> 'Aprovado',
            '3'=> 'Aprovado',//Digitando
            '4'=> 'Encerrado'
        ];

    //FORMATAÇÃO

    public function getTipoFormatadoAttribute()
    {
        return Disciplina_Autorizacao::TIPO[$this->tipo];
    }

    public function getSituacaoFormatadaAttribute()
    {
        return Disciplina_Autorizacao::SITUACAO[$this->situacao];
    }



    //FUNÇÕES DE RELACIONAMENTO
    public function user() {
        return $this->belongsTo('users', 'id', 'user_id');
    }

    public function disciplinas() {
        return $this->hasOne(Disciplina::class, 'id', 'disciplina');
    }
}
