<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Disciplina_Dispensada extends Model
{
    //
    protected $table='disciplinas_dispensadas';
    protected $primaryKey='id';

    public function dado()
    {
        return $this->hasOne(Dado::class,'matricula','aluno');
    }

    public function disciplinas()
    {
        return $this->hasOne(Disciplina::class,'id','disciplina');
    }

}
