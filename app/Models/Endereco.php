<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\Cidade;


class Endereco extends Model
{
    protected $connection = "alunos";
    protected $table = "endereco";

    public function dado() {
        return $this->belongsTo(Dado::class, 'endereco', 'codigo');
    }

    public function getCidade() {
        return $this->hasOne(Cidade::class, 'codigo', 'cidade');
    }
}
