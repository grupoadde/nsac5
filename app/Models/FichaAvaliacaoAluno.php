<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class FichaAvaliacaoAluno extends Model
{
    protected $table = 'ficha_avaliacao';
    public $timestamps=false;

    public function item() {
        return $this->belongsTo(FichaAvaliacaoItem::class, 'id', 'item');
    }

    public function dado() {
        return $this->belongsTo(Dado::class, 'matricula', 'aluno');
    }

    public function disciplina() {
        return $this->belongsTo(Disciplina::class, 'disciplina', 'id');
    }

}
