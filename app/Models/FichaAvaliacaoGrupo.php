<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class FichaAvaliacaoGrupo extends Model
{
    protected $table='ficha_grupo';
    public $timestamps=false;

    public function itens()
    {
        return $this->hasMany(FichaAvaliacaoItem::class,'grupo','id')->where('ativo',TRUE)->orderBy('descricao');
    }

}
