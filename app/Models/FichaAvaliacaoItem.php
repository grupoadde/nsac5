<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class FichaAvaliacaoItem extends Model
{
    protected $table='ficha_itens';
    public $timestamps=false;

    public function PossuiAluno($aluno, $disciplina, $bimestre) {
        return FichaAvaliacaoAluno::where('matricula', $aluno)
            ->where('disciplina', $disciplina)
            ->where('bim', $bimestre)
            ->where('cod_item', $this->id)
            ->first();
    }

    public function getPossuiAlunosAttribute() {
        return FichaAvaliacaoAluno::where('cod_item', $this->id)->count() > 0 ? true : false ;
    }

    public function grupopai()
    {
        return $this->belongsTo(FichaAvaliacaoGrupo::class,'grupo','id');
    }
}
