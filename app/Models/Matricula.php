<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Matricula extends Model
{
    protected $table = 'alunos.matriculas';

    protected $primaryKey = 'codigo';

    CONST situacaoRDM = [
        0 => 'Cursando',
        1 => 'Cancelado',
        2 => 'Desistente',
        3 => 'Transferido',
        4 => 'Terminou com Dependência',
        5 => 'Aprovado',
        6 => 'Reprovado',
        7 => 'Passou mas abriu mão'
    ];

    CONST situacaoRDF = [
        0 => 'Cursando',
        1 => 'Cancelada',
        2 => 'Desistente',
        3 => 'Transferida',
        4 => 'Terminou com Dependência',
        5 => 'Aprovada',
        6 => 'Reprovada',
        7 => 'Passou mas abriu mão'
    ];


    public function getSituacaoReadableAttribute() {
        return $this->dado->sexo == 0 ? $this::situacaoRDM[$this->situacao] : $this::situacaoRDF[$this->situacao];
    }

    //FUNÇÕES DE RELACIONAMENTO
    public function dado() {
        return $this->belongsTo(Dado::class,'aluno','matricula');
    }

    public function turma() {
        return $this->belongsTo(Turma::class, 'codigo', 'turma');
    }

    public function notas()
    {
        return $this->hasMany(Nota::class,'aluno','aluno');
    }
	
	public function notas_ano_atual($turma_tecnico,$turma_medio)
	{
		return Disciplina::where('turma',$turma_medio)->orwhere('turma',$turma_tecnico)
		->with(['notas'=>function($query)
			{
				$query->where('aluno',$this->aluno);
			}])->get();
	}

}