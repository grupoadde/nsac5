<?php

namespace App\Models;

use App\Models\Relations\NotaRelations;
use App\Models\Scopes\NotaScopes;
use Illuminate\Database\Eloquent\Model;
use App\Models\Presenters\NotaPresenter;
use Illuminate\Support\Facades\Log;

class Nota extends Model
{

    use NotaPresenter;
    use NotaRelations;
    use NotaScopes;

    protected $table='alunos.notas';
    protected $primaryKey='id';
    public $timestamps=false;

    protected $fillable=[
        'aluno',
        'disciplina',
        'nb1',
        'nb2',
        'nb3',
        'nb4'
    ];

    CONST REC = [
        0 => 'Não Compareceu',
        1 => 'Satisfatório',
        2 => 'Insatisfatório',
        3 => 'Não Aconteceu'
    ];

    public function scopeConselho($media, $faltas, $tipo)
    {
        $faltamaxima = floor($this->disciplina_atribuida->carga_horaria * $faltas /100);
/*        $query->with(['disciplina' => function ($query) {
           return $query->select('carga_horaria');
        }]);*/
        if ($tipo == 1) { //1º Bi
            Return $this::where( function($query) use ($media) {
                return $query
                    ->where('nb1', '<', $media)
                    ->where('rec1', '!=', 1);
            });
        } elseif($tipo == 2) { //2º Bi
            Return $this::where( function($query) use ($media) {
                return $query
                    ->where('nb2', '<', $media)
                    ->where('rec1', '!=', 1);
            });
        } elseif($tipo == 3) { //3º Bi
            Return $this::where( function($query) use ($media) {
                return $query
                    ->where('nb3', '<', $media)
                    ->where('rec2', '!=', 1);
            });
        } elseif($tipo == 4) { // 4º Bi
            Return $this::where(function ($query) use ($media) {
                return $query
                    ->where('nb4', '<', $media)
                    ->where('rec2', '!=', 1);
            });
        }

        Return $this::orWhere('fb1+fb2+fb3+fb', '>', $faltamaxima);
        //$query->orWhereRaw('(fb1+fb2+fb3+fb4) > trunc(disciplina.carga_horaria * ? / 100)', [$faltas]);

        return $query;

    }

    //Formatando Recuperação
    public function getrec1FormatadoAttribute($value) {
        return $this->REC[$value];
    }
    public function getrec2FormatadoAttribute($value) {
        return $this->REC[$value];
    }
	
	//Formatando Recuperação
	 public function getconselhoRec1FormatadoAttribute() {
        return Nota::REC[$this->rec1];
    }
    public function getconselhoRec2FormatadoAttribute() {
        return Nota::REC[$this->rec2];
    }

    public function getNotaFinalAttribute()
    {
        $notafinal = $this->nb1 + $this->nb2 + $this->nb3 + $this->nb4;
        return $notafinal/4;
    }


    /*public function Conselho($nota, $falta, $tipo) {
        if($tipo == 1) { //1º Bimestre
            $notaaluno = $this->nb1;
            $falta = $this->fb1;
        } elseif ($tipo == 2) { //2º Bimestre
            $notaaluno = $this->nb2;
            $falta = $this->fb2;
        } elseif ($tipo == 3) { //3º Bimestre
            $notaaluno = $this->nb3;
            $faltaaluno = $this->fb3;
        } elseif ($tipo == 4) { //4º Bimestre
            $notaaluno = $this->nb4;
            $faltaaluno = $this->fb4;
        } elseif ($tipo == 5) { //Conselho Parcial
            $notaaluno = 0;
            $faltaaluno = 0;
        } elseif ($tipo == 6) { //Conselho Final
            $notaaluno = 0;
            $faltaaluno = 0;
        }

        if (($notaaluno < $nota) || ($faltaaluno > $falta)) {
            return true;
        } else {
            return false;
        }

    }*/

}
