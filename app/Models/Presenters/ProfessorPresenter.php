<?php
/**
 * Created by PhpStorm.
 * User: Kuste
 * Date: 18/07/2017
 * Time: 16:06
 */

namespace App\Models\Presenters;
use App\Models\Professor;

trait ProfessorPresenter
{
    public function getSexoFormatadoAttribute() {
        return Professor::SEXO[$this->sexo];
    }

    public function getSituacaoFormatadoAttribute() {
        return Professor::SITUACAO[$this->situacao];
    }

    public function getFuncaoFormatadoAttribute() {
        return Professor::FUNCAO[$this->funcao];
    }

    public function getCpfFormatadoAttribute() {
        $valor = $this->cpf;
        if (!empty($valor)) {
            if (strlen($valor) == 11) {
                $valor = preg_replace('/(\d{3})(\d{3})(\d{3})(\d{2})/', '$1.$2.$3-$4', $valor);
            }
        }
        return $valor;
    }
}