<?php

namespace App\Models;

use App\Models\Presenters\ProfessorPresenter;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class Professor extends Model
{
    const SEXO = [
        0 => 'Masculino',
        1 => 'Feminino'
    ];

    const FUNCAO = [
        0 => 'D.E.M. I',
        1 => 'D.E.M. II',
        2 => 'Temporário'
    ];

    const SITUACAO = [
        0 => 'Normal',
        1 => 'Afastado',
        2 => 'Exonerado',
        3 => 'Aposentado'
    ];

    protected $fillable = [
        'nome',
        'email',
        'cpf',
        'rg',
        'apelido'
    ];

    use ProfessorPresenter;

    protected $table = 'professores';

    protected $primaryKey = 'codigo';

    public $timestamps = false;

    public function setCpfAttribute($value) {
        $this->cpf = preg_replace('/[^0:9]/','',$value);
    }

    public function DisciplinaAtribuida($disciplina) {
        $hoje = new Carbon();
        return $this->atribuicoes()->where('data_final', '>=', $hoje)->where('data_inicial', '<=', $hoje)->first()->disciplinas()->where('disciplinas.id', $disciplina)->first() == null ? false : true;
    }

    //FUNÇÕES DE RELACIONAMENTO
    public function telefones() {
        return $this->hasMany(Professor_Telefone::class, 'dono', 'codigo');
    }

    public function atribuicoes() {
        return $this->hasMany(Atribuicao::class,'professor', 'codigo');
    }

}
