<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Professor_Telefone extends Model
{
    protected $table = 'professores_telefone';

    protected $primaryKey = 'codigo';

    public $timestamps = false;

    public function professor()
    {
        return $this->belongsTo(Professor::class, 'codigo', 'dono');
    }
}
