<?php
/**
 * Created by PhpStorm.
 * User: Kuste
 * Date: 18/07/2017
 * Time: 16:02
 */

namespace App\Models\Relations;


use App\Models\Disciplina_Adaptacao;
use App\Models\Disciplina_Dependencia;
use App\Models\Disciplina_Dispensada;
use App\Models\FichaAvaliacaoAluno;
use App\Models\Matricula;
use App\Models\Nota;
use App\Models\Endereco;
use App\Models\Responsavel;

trait DadoRelations
{

    public function notas()
    {
        return $this->hasMany(Nota::class,'aluno','matricula');
    }

    public function matriculas()
    {
        return $this->hasMany(Matricula::class, 'codigo', 'matricula');
    }

    public function disciplinas_dependencia()
    {
        return $this->hasMany(Disciplina_Dependencia::class, 'aluno', 'matricula');
    }

    public function disciplinas_adaptacao()
    {
        return $this->hasMany(Disciplina_Adaptacao::class,'aluno', 'matricula');
    }

    public function disciplinas_dispensadas()
    {
        return $this->hasMany(Disciplina_Dispensada::class,'aluno', 'matricula');
    }

    public function fichaavaliacaoaluno()
    {
        return $this->hasMany(FichaAvaliacaoAluno::class,'matricula','matricula');
    }

    public function getEndereco() {
        return $this->hasOne(Endereco::class, 'codigo', 'endereco');
    }

    public function responsaveis() {
        return $this->hasMany(Responsavel::class, 'matricula', 'matricula');
    }
}