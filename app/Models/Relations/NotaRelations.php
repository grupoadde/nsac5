<?php
/**
 * Created by PhpStorm.
 * User: Kuste
 * Date: 18/07/2017
 * Time: 16:00
 */

namespace App\Models\Relations;

use App\Models\Dado;
use App\Models\Disciplina;
use App\Models\Disciplina_Adaptacao;
use App\Models\Disciplina_Dependencia;
use App\Models\Disciplina_Dispensada;

trait NotaRelations
{

    public function dado()
    {
        return $this->belongsTo(Dado::class,'aluno','matricula')->orderBy('nome');
    }

    public function disciplina_atribuida()
    {
        return $this->belongsTo(Disciplina::class,'disciplina','id');
    }

    //Disciplina_Adaptacao
    public function disciplina_adaptacao()
    {
        return $this->belongsTo(Disciplina_Adaptacao::class,'aluno','aluno');
    }

    //Disciplina_Dependencia
    public function disciplina_dependencia()
    {
        return $this->belongsTo(Disciplina_Dependencia::class,'aluno','aluno');
    }

    //Disciplina_Dispensada
    public function disciplina_dispensada()
    {
        return $this->belongsTo(Disciplina_Dispensada::class,'aluno','aluno');
    }

}