<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Responsavel extends Model
{
    public function telefones() {
        return $this->hasMany(ResponsavelTelefone::class, 'id_responsavel', 'codigo');
    }
}
