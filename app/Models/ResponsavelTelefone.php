<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ResponsavelTelefone extends Model
{
    public function responsavel() {
        return $this->belongsTo(Responsavel::class, 'codigo', 'id_responsavel');
    }
}
