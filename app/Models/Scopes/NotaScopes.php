<?php
/**
 * Created by PhpStorm.
 * User: Kuste
 * Date: 18/07/2017
 * Time: 16:24
 */

namespace App\Models\Scopes;


trait NotaScopes
{

    public function scopeAluno($query, $aluno) {
        return $query->where('aluno', $aluno);
    }

}