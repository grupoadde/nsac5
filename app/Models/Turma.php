<?php

namespace App\Models;

use App\Models\Relations\TurmaRelations;
use Illuminate\Database\Eloquent\Model;

class Turma extends Model
{

    use TurmaRelations;

    protected $table = 'turmas';

    protected $primaryKey = 'codigo';

    public function turmaintegrada() {
        return $this->hasOne(Turma::class, 'turma_integrada', 'codigo');
    }

    public function getNomeIntegradoAttribute() {
        $turma2 = $this->turmaintegrada;
        if(isset($turma2)) {
            if ($this->cursos->tipo == 0) {
                return $this->nomenclatura.'/'.$turma2->nomenclatura;
            } else {
                return $turma2->nomenclatura.'/'.$this->nomenclatura;
            }
        } else {
            return $this->nomenclatura;
        }
    }

}


/*update turmas
set
turma_integrada = ti.turma_tecnico
from
(select * from turmas_integradas) as ti
where
turmas.codigo = ti.turma_medio*/