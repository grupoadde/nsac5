function sendEmailTo(email) {
  $('#to').val(email);
  $('#from').val(email);
  $('#modalEmail').modal('toggle');
}

$(document).ready(function() {
  $('#modalEmail').submit(function(e) {
     e.preventDefault();
     alert("Enviando...");
     $('#modalEmail').modal('toggle');
  });

  $('#message').keyup(function () {
    var letters = $('#message').val().length;
    $('#length').html('<small>' + letters + "/250</small>");
  });
});
