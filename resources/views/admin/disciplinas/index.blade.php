@extends('general.layouts.navsidebar')
@section('content')

    {{--    <div class="container">
            <div class="row">--}}
    <div >
        <div class="panel panel-default">
            <div class="panel-heading">
                <h3>
                    Disciplinas
                    <a href="{{ route('disciplinas.create') }}" class="btn btn-default pull-right">Nova Disciplina</a>
                </h3>
            </div>
            <div class="panel-heading">Página {{ $professores->currentPage() }} de {{ $professores->lastPage() }}</div>

            @foreach($turmas as $turma)

            <table class="table table-bordered table-striped">

                <thead>
                <tr>
                    <th>Codigo</th>
                    <th>Descrição</th>
                    <th>Operações</th>
                </tr>
                </thead>

                <tbody>

                @foreach ($turma->disciplinas as $disciplina)
                    <tr>
                        <td>{{ $disciplina->CodigoCTI }}</td>
                        <td>{{ $professor->descricao }}</td>
                        {{--<td>{{ $professor->cpf_formatado }}</td>--}}

                        <td>
                            <a href="{{ route('professores.show', $professor->codigo) }}" class="btn btn-info pull-left" style="margin-right: 3px;">Ver</a>
                            <a href="{{ route('professores.edit', $professor->codigo) }}" class="btn btn-info" role="button">Editar</a>
                            <a href="{{ route('professores.destroy', $professor->codigo) }}" class="btn btn-danger" role="button">Apagar</a>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
            @endforeach

        </div>
        <div class="text-center">
            {!! $professores->links() !!}
        </div>

    </div>
    {{--        </div>
        </div>--}}
@endsection