@extends('adminlte::page')

@section('title', 'Perfis')

@section('content_header')
    <h1>{{--<i class="fa fa-key"></i>--}} Grupos de Ficha de Avaliação <small>Adicionando Grupo</small></h1>
@stop

@section('content')

    <div class="box box-primary">
        <div class="box-header with-border">
            <h3 class="box-title">Novo Grupo</h3>
        </div>

        <div class="box-body">

            {{ Form::open(array('url' => 'fichagrupos/store')) }}

            @include('professor.ficha-avaliacao.grupos._form')

        </div>
        <div class="box-footer">
            {{ Form::submit('Criar Grupo', array('class' => 'btn btn-primary')) }}

            {{ Form::close() }}
        </div>
    </div>
@endsection