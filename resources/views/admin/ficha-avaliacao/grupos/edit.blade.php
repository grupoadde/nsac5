@extends('adminlte::page')

@section('title', 'Perfis')

@section('content_header')
    <h1>{{--<i class="fa fa-key"></i>--}} Grupos de Ficha de Avaliação <small>Editando Grupo</small></h1>
@stop

@section('content')

    <div class="box box-primary">
        <div class="box-header with-border">
            <h3 class="box-title"> Editar Grupo: {{$grupo->descricao}}</h3>
        </div>

        <div class="box-body">

            {{ Form::model($grupo, array('route' => array('fichagrupos.update', $grupo->id), 'method' => 'PUT')) }}

            @include('admin.ficha-avaliacao.grupos._form')

            <hr>
            <div class="form-group">
                {{ Form::label('descricao', 'Itens do Grupo') }}
                <table class="table table-bordered table-striped">
                    <thead>
                    <tr>
                        <th>Nome do Item</th>
                        <th>Texto</th>
                        <th>Operação</th>
                    </tr>
                    </thead>

                    <tbody>
                    @forelse ($itens as $item)
                        <tr>
                            <td>{{ $item->descricao }}</td>
                            <td> @if($item->tem_texto) Sim @else Não @endif </td>
                            <td>
                                <a href="{{ Route('fichaitens.edit', $item->id) }}" class="btn btn-info pull-left" style="margin-right: 3px;">Editar</a>

                                <a href="{{ Route('fichaitens.destroy',['id' => $item->id]) }}" class="btn btn-danger pull-left" style="margin-right: 3px;">
                                    @if($item->PossuiAlunos == FALSE)
                                        Deletar
                                    @else
                                        @if($item->ativo==FALSE)
                                            Ativar
                                        @else
                                            Desativar
                                        @endif
                                    @endif
                                </a>

                            </td>
                        </tr>
                    @empty
                        <td colspan="3">Nenhum Item cadastrado até o momento</td>
                    @endforelse
                    </tbody>
                </table>
                <a href="{{ Route('fichaitens.create',$grupo->id) }}" class="btn btn-success">Novo Item</a>
            </div>
        </div>
        <div class="box-footer">
            {{ Form::submit('Salvar Alterações', array('class' => 'btn btn-primary')) }}
            {{ Form::close() }}
        </div>
    </div>

@endsection