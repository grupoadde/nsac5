@extends('adminlte::page')

@section('title', 'Itens da Ficha de Avaliação')

@section('content_header')
    <h1>{{--<i class="fa fa-key"></i>--}} Itens do Grupo de Ficha de Avaliação <small>Adicionando Item</small></h1>
@stop

@section('content')

    <div class="box box-primary">
        <div class="box-header">
            <h3 class="box-title">Editar Item: {{$item->descricao}}</br><small>Grupo: {{ $item->grupopai->descricao }}</small></h3>
        </div>

        <div class="box-body">

            {{ Form::model($item, array('route' => array('fichaitens.update', $item->id), 'method' => 'PUT')) }}

        @include('admin.ficha-avaliacao.itens._form')

        </div>
        <div class="box-footer">

        {{ Form::submit('Salvar Alteração', array('class' => 'btn btn-primary')) }}

        {{ Form::close() }}

        </div>

    </div>




@endsection