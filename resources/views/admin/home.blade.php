@extends('adminlte::page')

@section('title', 'AdminLTE')

@section('content_header')
    <h1>Dashboard</h1>
@stop

@section('content')
    <p>You are logged in!</p>

    <button class="btn btn-default" data-toggle="control-sidebar">Toggle Right Sidebar</button>
@stop