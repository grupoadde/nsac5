<div class="box-body">
    <div class="form-group">
        {{ Form::label('name', 'Nome da Permissão') }}
        {{ Form::text('name', null, array('class' => 'form-control')) }}
    </div>
    <div class="form-group">
        <label>Atribuir permissão aos perfis:</label>
        @if($roles->isEmpty())
            Não há Perfis cadastrados ainda!
        @else
            @foreach ($roles as $role)
                <div class="checkbox">
                    <label>
                    {{ Form::checkbox('roles[]',  $role->id ) }}
                    {{ ucfirst($role->name) }}
                    </label>
                </div>
            @endforeach
        @endif
    </div>
</div>