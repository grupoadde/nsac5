@extends('adminlte::page')

@section('title', 'Nova Permissão')

@section('content_header')
    <h1><i class="fa fa-key"></i> Permissões de Usuário <small>Adicionar Permissão</small></h1>
@stop

@section('content')

    <div class="box box-primary">
        <div class="box-header with-border">
            <h3 class="box-title">Nova Permissão</h3>
        </div>

        {{ Form::open(['route' => 'admin.permissions.store']) }}

        @include('admin.permissions._form')

        <div class="box-footer with-border">
            {{ Form::submit('Criar Permissão', array('class' => 'btn btn-primary')) }}
            {{ Form::close() }}
        </div>

    </div>

@endsection