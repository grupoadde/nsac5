@extends('adminlte::page')

@section('title', 'Permissões de Usuário')

@section('content_header')
    <h1><i class="fa fa-key"></i> Permissões de Usuário <small>Editando Permissão</small></h1>
@stop

@section('content')

    <div class="box box-primary">
        <div class="box-header with-border">
            <h3 class="box-title"> Editar Permissão: {{$permission->name}}</h3>
        </div>

        <div class="box-body">
            {{ Form::model($permission, array('route' => array('admin.permissions.update', $permission->id), 'method' => 'PUT')) }}
            @include('admin.permissions._form')

        </div>
        <div class="box-footer with-border">
            {{ Form::submit('Salvar', array('class' => 'btn btn-primary')) }}
            {{ Form::close() }}
        </div>

    </div>

@endsection