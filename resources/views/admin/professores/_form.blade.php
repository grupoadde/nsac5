<div class="form-group">
    {{ Form::label('nome', 'Nome') }}
    {{ Form::text('nome', null, array('class' => 'form-control')) }}
</div>

<div class="form-group">
    {{ Form::label('apelido', 'Apelido') }}
    {{ Form::text('apelido', null, array('class' => 'form-control')) }}
</div>

<div class="form-group">
    {{ Form::label('email', 'E-mail') }}
    {{ Form::email('email', null, array('class' => 'form-control')) }}
</div>

<div class="form-group">
    {{ Form::label('sexo', 'Sexo') }}<br>
    {{ Form::select('sexo', App\Models\Professor::SEXO, null, array('class' => 'form-control')) }}
</div>

<div class="form-group">
    {{ Form::label('rg', 'RG') }}
    {{ Form::text('rg', null, array('class' => 'form-control')) }}
</div>

<div class="form-group">
    {{ Form::label('cpf', 'CPF') }}
    {{ Form::text('cpf', null, array('class' => 'form-control')) }}
</div>

<div class="form-group">
    {{ Form::label('carga_horaria', 'Carga Horária') }}
    {{ Form::number('carga_horaria', null,  array('class' => 'form-control')) }}
</div>

<div class="form-group">
    {{ Form::label('rd', 'RD') }}
    {{ Form::number('rd', null,  array('class' => 'form-control')) }}
</div>

<div class="form-group">
    {{ Form::label('funcao', 'Função') }}<br>
    {{ Form::select('funcao', App\Models\Professor::FUNCAO, null, array('class' => 'form-control')) }}
</div>

<div class="form-group">
    {{ Form::label('situacao', 'Situação') }}<br>
    {{ Form::select('situacao', App\Models\Professor::SITUACAO, null, array('class' => 'form-control')) }}
</div>