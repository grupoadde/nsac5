@extends('adminlte::page')

@section('title', 'Professores')

@section('content_header')
    <h1>Professores <small>Novo Professor</small></h1>
@stop

@section('content')

        <div class="box box-primary">
            <div class="box-header with-border">
                <h3 class="box-title">Cadastrar novo professor</h3>
            </div>
            <div class="box-body">
                {{-- Using the Laravel HTML Form Collective to create our form --}}
                {{ Form::open(['route' => 'admin.professores.store', 'class' =>'form']) }}

                @include('admin.professores._form')

                <div class="form-group">
                    {{ Form::submit('Cadastrar Professor', array('class' => 'btn btn-success btn-lg btn-block')) }}
                    {{ Form::close() }}
                </div>
            </div>
        </div>
    {{--</div>--}}

@endsection