@extends('adminlte::page')

@section('title', 'Professores')

@section('content_header')
    <h1>Professores <small>Editar Professor</small></h1>
@stop

@section('content')

        {{--<div class="col-md-8 col-md-offset-2">--}}
        <div class="box box-primary">
            <div class="box-header with-border">
            <h3 class="box-title">Editar Professor</h3></div>

            <div class="box-body">

                {{ Form::model($professor, ['route' => ['admin.professores.update', $professor->codigo], 'method' => 'PUT', 'class' => 'form']) }}

                @include('admin.professores._form')

                {{ Form::submit('Salvar', ['class' => 'btn btn-primary']) }}

                {{ Form::close() }}

            </div>
        </div>

@endsection