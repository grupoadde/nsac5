<div class="box-body">
    <div class="form-group">
        {{ Form::label('name', 'Nome do Perfil') }}
        {{ Form::text('name', null, array('class' => 'form-control')) }}
    </div>
    <div class="form-group">
        <label>Atribuir Permissões</label>
        @foreach ($permissions as $permission)
            <div class="checkbox">
                <label>
                {{Form::checkbox('permissions[]',  $permission->id ) }}
                {{ ucfirst($permission->name) }}
                </label>
            </div>
        @endforeach
    </div>
</div>