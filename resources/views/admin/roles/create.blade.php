@extends('adminlte::page')

@section('title', 'Perfis de Usuário')

@section('content_header')
    <h1><i class="fa fa-key"></i> Perfis de Usuário <small>Adicionar Perfil</small></h1>
@stop

@section('content')

    <div class="box box-primary">
        <div class="box-header with-border">
            <h3 class="box-title">Novo Perfil</h3>
        </div>

        {{ Form::open(['route' => 'admin.roles.store']) }}

        @include('admin.roles._form')

        <div class="box-footer with-border">
            {{ Form::submit('Criar Perfil', array('class' => 'btn btn-primary')) }}

            {{ Form::close() }}
        </div>
    </div>

@endsection