@extends('adminlte::page')

@section('title', 'Perfis de usuário')

@section('content_header')
    <h1><i class="fa fa-key"></i>Perfis de Usuário <small>Editando Perfil</small></h1>
@stop

@section('content')

    <div class="box box-primary">
        <div class="box-header with-border">
            <h3 class="box-title"> Editar Perfil: {{$role->name}}</h3>
        </div>

        @include('flash::message')

        {{ Form::model($role, ['route' => ['admin.roles.update', $role->id], 'method' => 'PUT', 'role' => 'form']) }}

        @include('admin.roles._form')

        <div class="box-footer with-border">
            {{ Form::submit('Salvar', array('class' => 'btn btn-primary')) }}

            {{ Form::close() }}
        </div>
    </div>

@endsection