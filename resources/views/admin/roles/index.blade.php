@extends('adminlte::page')

@section('title', 'Perfis de usuário')

@section('content_header')
    <h1><i class="fa fa-key"></i> Perfis de Usuário <small>Lista dos Perfis Cadastrados</small></h1>
@stop

@section('content')

        <div class="box box-primary">
{{--            <div class="box-header">
                <a href="{{ route('users.index') }}" class="btn btn-default pull-right">Usuários</a>
                <a href="{{ route('permissions.index') }}" class="btn btn-default pull-right">Permissões</a>
            </div>--}}
            <div class="box-body">
            <table class="table">
                <thead>
                <tr>
                    <th>Perfil</th>
                    <th>Permissões</th>
                    <th class="text-center" colspan="2">Operações</th>
                </tr>
                </thead>
                <tbody>
                    @foreach ($roles as $role)

                        <tr>
                            <td>{{ $role->name }}</td>
                            <td>{{ str_replace(array('[',']','"'),'', $role->permissions()->pluck('name')) }}</td>
                            <td>
                                <a href="{{ Route('admin.roles.edit',$role->id) }}" class="btn btn-info pull-right">Editar</a>
                            </td>
                            <td>
                                {!! Form::open(['method' => 'DELETE', 'route' => ['admin.roles.destroy', $role->id] ])!!}
                                {!! Form::submit('Apagar', ['class' => 'btn btn-danger pull-left'])!!}
                                {!! Form::close()!!}
                            </td>
                        </tr>

                    @endforeach
                </tbody>

            </table>
            </div>
            <div class="box-footer with-border">
                <a href="{{ Route('admin.roles.create') }}" class="btn btn-success">Novo Perfil</a>
            </div>
        </div>
@endsection