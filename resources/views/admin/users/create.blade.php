{{-- \resources\views\users\create.blade.php --}}
@extends('adminlte::page')

@section('title', '| Criar Usuário')

@section('content')

    <div>

        <h3><i class='fa fa-user-plus'></i> Criar Usuário</h3>
        <hr>

        {{ Form::open(array('route' => 'admin.users.store')) }}

        <div class="form-group">
            {{ Form::label('name', 'Nome do Usuário') }}
            {{ Form::text('name', '', array('class' => 'form-control')) }}
        </div>
        <div class="form-group">
            {{ Form::label('email', 'Email') }}
            {{ Form::email('email', '', array('class' => 'form-control')) }}
        </div>
        <div class='form-group'>
            @foreach ($roles as $role)
                {{ Form::checkbox('roles[]',  $role->id ) }}
                {{-- {{ Form::label($role->name, ucfirst($role->name)) }}<br> --}}
                {{ ucfirst($role->name) }} <br>
            @endforeach
        </div>
        <div class="form-group">
            {{ Form::label('password', 'Senha') }}<br>
            {{ Form::password('password', array('class' => 'form-control')) }}
        </div>
        <div class="form-group">
            {{ Form::label('password', 'Confirmar Senha') }}<br>
            {{ Form::password('password_confirmation', array('class' => 'form-control')) }}
        </div>
        {{ Form::submit('Adicionar', array('class' => 'btn btn-primary')) }}

        {{ Form::close() }}

    </div>

@endsection