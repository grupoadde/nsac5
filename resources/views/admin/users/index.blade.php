@extends('adminlte::page')

@section('title', 'Usuário')

@section('content_header')
    <h1>Usuários <small>Lista dos Usuários Cadastrados</small></h1>
@stop

@section('content')

    <div class="box box-primary">
        {{--            <div class="box-header">
                        <a href="{{ route('users.index') }}" class="btn btn-default pull-right">Usuários</a>
                        <a href="{{ route('permissions.index') }}" class="btn btn-default pull-right">Permissões</a>
                    </div>--}}
        <div class="box-body">

    {{--<div >
        <h3><i class="fa fa-users"></i> Administração de Usuários <a href="{{ route('admin.roles.index') }}" class="btn btn-default pull-right">Perfis</a>
            <a href="{{ route('admin.permissions.index') }}" class="btn btn-default pull-right">Permissões</a></h3>
        <hr>
      <div class="table-responsive">
          --}}    <table class="table table-bordered table-striped">

                <thead>
                <tr>
                    <th>Nome</th>
                    <th>Email</th>
                    {{-- <th>Cadastro em</th> --}}
                    <th>Perfis</th>
                    <th>Operações</th>
                </tr>
                </thead>

                <tbody>
                @foreach ($users as $user)
                    <tr>
                        <td>{{ $user->name }}</td>
                        <td>{{ $user->email }}</td>
                        {{--<td>{{ $user->created_at->format('F d, Y h:ia') }}</td> --}}
                        <td>{{ $user->roles()->pluck('name')->implode(' ') }}</td>{{-- Retrieve array of roles associated to a user and convert to string --}}
                        <td style="white-space: nowrap;">
                            <a href="{{ route('admin.users.edit', $user->id) }}" class="btn btn-info pull-left" style="margin-right: 3px;">Editar</a>
                            {!! Form::open(['method' => 'DELETE', 'route' => ['admin.users.destroy', $user->id] ]) !!}
                            {!! Form::submit('Apagar', ['class' => 'btn btn-danger']) !!}
                            {!! Form::close() !!}
                        </td>
                    </tr>
                @endforeach
                </tbody>

            </table>
        </div>

        <div class="box-footer">
            <a href="{{ route('admin.users.create') }}" class="btn btn-success">Adicionar Usuário</a>
        </div>

    </div>

@endsection