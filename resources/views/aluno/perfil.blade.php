@extends('adminlte::page')

@section('title', 'Perfil')

@section('content_header')
    <h1>Perfil <small>Informações do aluno</small></h1>
@stop

@section('content')

    <div class="row">
        <div class="col-md-4">
            <div class="box box-primary">
                <div class="box-body box-profile">
                <h3 class="profile-username text-center">{{ $perfil->nome }}</h3>
                <ul class="list-group list-group-unbordered">
                    <li class="list-group-item">
                        <b>Apelido</b> <span class="pull-right">{{ ($perfil->apelido == '') ? '--' : $perfil->apelido }}</span>
                    </li>
                    <li class="list-group-item">
                        <b>Data de nascimento</b> <span class="pull-right">{{ $perfil->data_de_nascimento }}</span>
                    </li>
                    <li class="list-group-item">
                        <b>RG</b> <span class="pull-right">{{ $perfil->rg }}</span>
                    </li>
                    <li class="list-group-item">
                        <b>CPF</b> <span class="pull-right">{{ $perfil->cpf }}</span>
                    </li>
                </ul>
                </div>
            </div>
        </div>
        <div class="col-md-8">
            <div class="nav-tabs-custom">
                <ul class="nav nav-tabs">
                    <li class="active">
                        <a href="#endereco" data-toggle="tab">Endereço</a>
                    </li>
                    <li>
                        <a href="#responsaveis" data-toggle="tab">Responsáveis</a>
                    </li>
                </ul>
                <div class="tab-content">
                    <div class="tab-pane active" id="endereco">
                        <div class="row">
                            <div class="col-md-6">
                                <form>
                                    <div class="form-group">
                                        <label for="rua">Logradouro</label>
                                        <input type="text" class="form-control" value="{{ $perfil->getEndereco->logradouro }}" id="rua" max-lenght="60" placeholder="Ex: Avenida Nações Unidas" disabled>
                                    </div>
                                    <div class="form-group">
                                        <label for="numero">Numero</label>
                                        <input type="number" class="form-control" value="{{ $perfil->getEndereco->numero }}" id="numero" placeholder="Ex: 652" disabled>
                                    </div>
                                    <div class="form-group">
                                        <label for="complemento">Complemento</label>
                                        <input type="text" class="form-control" value="{{ ($perfil->getEndereco->complemento == '') ? 'Não disponível' : $perfil->getEndereco->complemento  }}" id="complemento" max-lenght="20" placeholder="Ex: Próximo à padaria XY" disabled>
                                    </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Cidade</label>
                                    <select id="cidade" class="form-control" disabled>
                                        @foreach($cidades as $cidade)
                                            <option value="{{ $cidade->codigo }}" {{ ($cidade->codigo == $perfil->getEndereco->getCidade->codigo) ? 'selected' : '' }} >{{ $cidade->nome }}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="form-group">
                                        <label for="bairro">Bairro</label>
                                        <input type="text" class="form-control" value="{{ $perfil->getEndereco->bairro }}" id="bairro" max-lenght="20" placeholder="Ex: Araruna, etc..." disabled>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                    <div class="tab-pane" id="responsaveis">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="box box-widget widget-user-2">
                                    <div class="widget-user-header bg-blue">
                                        <h3 class="widget-user-username" style="margin: 0 0 0 0">Nome</h3>
                                    </div>
                                    <div class="box-footer no-padding">
                                        <ul class="nav nav-stacked">
                                            <li><a href="">asdas</a></li>
                                        </ul>
                                    </div>
                                </div>    
                            </div>
                        </div>                                       
                    </div>  
                </div>
            </div>
        </div>
    </div>

@endsection