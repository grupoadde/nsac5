<html>

<head>

    <title>NSAC</title>

    <!-- Scripts -->
    <script>
        window.Laravel = {!! json_encode([
            'csrfToken' => csrf_token(),
        ]) !!};
    </script>

    <!-- Meta -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Favicon -->
    <link rel="shortcut icon" href="{{ asset('imgs/favicon.png') }}">

    <!-- CSS -->
    <link rel="stylesheet" href="{{ asset('css/bootstrap.css') }}">
    <link rel="stylesheet" href="{{asset('css/bootstrap-theme.css')}}">
    <link rel="stylesheet" href="{{asset('css/color-theme-blue.css')}}">
    <link rel="stylesheet" href="{{asset('css/style.css')}}">


    <!-- Javascript -->
    <script src="{{asset('js/jquery-3.2.1.js')}}"></script>
    <script src="{{asset('js/bootstrap.js')}}"></script>
    <script src="{{asset('js/scripts.js')}}"></script>

    <!-- Select -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.12.2/css/bootstrap-select.min.css">
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.12.2/js/bootstrap-select.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.12.2/js/i18n/defaults-*.min.js"></script>

</head>

<style>
    input[type=number]::-webkit-outer-spin-button,
    input[type=number]::-webkit-inner-spin-button {
        -webkit-appearance: none;
        margin: 0;
    }

    input[type=number] {
        -moz-appearance:textfield;
    }
</style>

<script type="text/javascript">
	function aux_inicial()
	{
		document.getElementById('aux').value=0;
	}
</script>

<body>

<div>
    <nav class="navbar navbar-dark bg-primary">
        <br>
        <a class="navbar-brand" style="color: white" href="{{route('home')}}">Nsac</a>
		
        <form id="requisitos_conselhobimestral" class="form-inline pull-xs-right" style="color: black" action="{{route('professor.conselhobimestral.show')}}" method="post">
            {{ csrf_field() }}
			
            @if(!ISSET($aux)) @php $aux=0; @endphp @endif
            @php $selecionado_bimestre=" "; $selecionado_turma=" ";@endphp

            <input type="hidden" name="aux" id="aux" value="{{$aux}}">

            <label style="color: white">Nota Mínima</label>
            <input type="number" name="nota_minima" step="0.1" min="0" max="10" value="{{$dados['nota_minima']}}" required>
            &nbsp;&nbsp;

            <label style="color: white">Faltas</label>
            <input type="number" name="falta_minima" min="0" max="50" value="{{$dados['falta']}}"required>
            &nbsp;&nbsp;
            <label style="color: white">Bimestre</label>
            <select name="bimestre_atual" required onchange="aux_inicial()">
                @for($a=1;$a<=4;$a++)

                    @if($dados['bimestre']==$a) @php $selecionado_bimestre=" selected"; @endphp @endif

                    <option value="{{$a}}" {{$selecionado_bimestre}}> {{$a}}º Bimestre </option>

                    @php $selecionado_bimestre=" "; @endphp
                @endfor
            </select>
            &nbsp;&nbsp;
            <label style="color: white">Turma</label>
            <select name="turma" required onchange="aux_inicial()">
                @foreach( Sistema::turmastecnico_teste() as $turma)
                    @if(ISSET($dados['turma']->codigo))
                        @if($dados['turma']->codigo==$turma->codigo)
                            @php $selecionado_turma="selected";@endphp
                        @endif
                    @endif
                        <option value="{{$turma->codigo}}" {{$selecionado_turma}}> {{$turma->NomeIntegrado}} </option>
                    @php $selecionado_turma=" "; @endphp
                @endforeach
            </select>
			
            <input type="submit" style="color: blue" value="Enviar">
        </form>
    </nav>
</div>

@if(Route::currentRouteName() == 'professor.conselhobimestral.show')
	@include('coordenador.conselhobimestral.show')
@endif

</body>
</html>