@extends('adminlte::page')

@section('title', 'NSac Online')

@section('content_header')
    <h1>ERRO!</h1>
@stop

@section('content')
    <div class="box box-danger">
        <div class="box-body">
            <p class="text-center">Erro 401 - Acesso NEGADO!</p>
        </div>
    </div>
@stop
