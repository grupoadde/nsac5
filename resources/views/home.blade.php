@extends('adminlte::page')

@section('title', 'Professor')

@section('content_header')
    <h1>NSac
        <small>{{date('d/m/Y')}}</small>
    </h1>
@stop


@section('content')
<!--
    <script>
        var jumboHeight = $('.jumbotron').outerHeight();
        function parallax(){
            var scrolled = $(window).scrollTop();
            $('.bg').css('height', (jumboHeight-scrolled) + 'px');
        }

        $(window).scroll(function(e){
            parallax();
        });
    </script>
    <div class="bg" style="
          background: url('../imgs/fachada.jpg') no-repeat center center;
            position: fixed;
          width: 100%;
          height: 350px; /*same height as jumbotron */
          top:0;
          left:0;
          z-index: -1;"
    >

    </div>
    <div class="jumbotron" style="
          height: 350px;
          color: white;
          text-shadow: #444 0 1px 1px;
          background: transparent;

    ">
        <h3>Bem vindo ao NSac do Colégio Técnico Industrial "Prof. Isaac Portal Roldán"</p>
        </h3>
        <p class="lead">+ Parallax Effect using jQuery</p>
    </div>


    -->


    <div class="box">
        <div class="box-body" style="background: url('../imgs/fachada.png') no-repeat center center; z-index: -1; background-size:cover;">

                <div class="col-sm-6" style=" vertical-align: middle;">
                    <p class="h3 text-center" style=" color: white; text-shadow: #444 0 1px 1px;">
                        Bem vindo(a) ao NSac do Colégio Técnico Industrial "Prof. Isaac Portal Roldán"</p>

                </div>

            <div class="col-sm-6" style="vertical-align: middle; ">

                <img class="img-responsive" src="../imgs/cti_icon.png" alt="Logo CTI">
            </div>

        </div>
    </div>


        <!-- Feature -->
        <div class="container-fluid">
            <div class="row">
                <a href="https://pt-br.facebook.com/cti.unesp.bauru/" target="_blank" style="color: black;">
                    <div class="col-sm-4">
                        <div class="info-box">


                            <span class="info-box-icon bg-aqua"><i class="fa fa-facebook"></i></span>

                            <div class="info-box-content">
                                <span class="info-box-text">Facebook</span>
                                <span class="info-box-number">Acompanhe a página da escola para novidades                           </span>
                            </div>
                            <!-- /.info-box-content -->
                        </div>
                    </div>
                </a>

                <a href="http://www.cti.feb.unesp.br/" target="_blank" style="color: black;">
                    <div class="col-sm-4">
                        <div class="info-box">
                            <span class="info-box-icon bg-aqua"><i class="fa fa-mouse-pointer"></i></span>

                            <div class="info-box-content">
                                <span class="info-box-text">Site do CTI</span>
                                <span class="info-box-number">Acesse o site da escola para mais informações</span>
                            </div>
                            <!-- /.info-box-content -->
                        </div>
                    </div>
                </a>

                <a href="https://unesp.br" target="_blank" style="color: black;">
                    <div class="col-sm-4">
                        <div class="info-box">
                            <span class="info-box-icon bg-aqua"><i class="fa  fa-university"></i></span>

                            <div class="info-box-content">
                                <span class="info-box-text">Site da UNESP</span>
                                <span class="info-box-number">Acesse o site da universidade</small>
                            </span>
                            </div>
                            <!-- /.info-box-content -->
                        </div>
                    </div>
                </a>
            </div>



        </div>





@stop