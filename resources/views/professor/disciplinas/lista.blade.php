<html>

<head>
    <title>{{$dados['disciplina']->CodigoCti.'_'.$dados['disciplina']->abreviacao.'_'.date('Y')}}</title>
    <style>
        @php
            include(public_path().'/css/bootstrap.css');
        @endphp
    </style>
</head>

<body >



<div class="text-center">

    <img src="imgs/cabecalho.jpg" style="width: 100%;">


    <h3 style="font-family: Helvetica !important; font-weight: bold;">
        {{$dados['disciplina']->CodigoCti}} - {{$dados['disciplina']->abreviacao}}
        <small style="font-size: 0.8em;">
            {{$dados['titulo']}}
        </small>
    </h3>


    <table class="table-responsive">
        <thead>
        <tr>

            <th class="col-sm-1">Nº</th>
            <th class="col-sm-2">R.A.</th>
            <th class="col-sm-5" style="width: 300px;">Nome</th>
            <th class="col-sm-1"></th>
            <th class="col-sm-3">Assinatura</th>

        </tr>
        </thead>
        <tbody style="font-size:0.95em;">
        @foreach($dados['alunos_matriculados'] as $aluno)

            <tr>

                <td class="col-sm-1">{{$aluno->ordem}}</td>
                <td class="col-sm-2">{{$aluno->dado->matricula}}</td>
                <td class="col-sm-6">{{$aluno->dado->nome}}</td>
                <td class="col-sm-1"></td>
                <td class="col-sm-3">_____________________________</td>

            </tr>




        @endforeach
        </tbody>
    </table>

</div>
</body>


</html>



