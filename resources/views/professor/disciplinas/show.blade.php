@extends('adminlte::page')

@section('title', 'Disciplinas')

@section('content_header')
    <h1>Disciplinas
        <small>Painel de Controle</small>
    </h1>
@stop

@section('content')
    <div class="box box-default">

        <div class="box-header">
            <h3 class="box-title">
                <strong>{{$disciplina->CodigoCti}}</strong> - {{ $disciplina->descricao }}
            </h3>
        </div>

        <div class="box-body">

            <div id="descricao">
                <div class="box-header with-border">
                    <i class="fa  fa-folder-o"></i>

                    <h3 class="box-title">Descrição</h3>
                </div>
                <div class="box-body">
                    <div class="col-sm-8">
                        <dl class="dl-horizontal">
                            <dt>Código</dt>
                            <dd>{{$disciplina->CodigoCti}}</dd>
                            <dt>Descrição</dt>
                            <dd>{{$disciplina->descricao}}</dd>
                            <dt>Abreviação</dt>
                            <dd>{{$disciplina->abreviacao}}</dd>
                            <dt>Carga horária</dt>
                            <dd>{{$disciplina->carga_horaria}} horas</dd>
                            <dt>Turma</dt>
                            <dd>{{$disciplina->turmas->nomenclatura}}</dd>
                            <dt>Ano</dt>
                            <dd>{{$disciplina->turmas->ano}}</dd>
                        </dl>
                    </div>
                    <div class="col-sm-4">
                        <div class="small-box bg-blue-gradient">
                            <div class="inner">
                                <h3>{{$turma->alunos->count()}}</h3>
                                <p>Alunos</p>
                            </div>
                            <div class="icon">
                                <i class="ion ion-person-stalker"></i>
                            </div>
                        </div>
                        <div class="small-box bg-blue-gradient">
                            <div class="inner">
                                <h3>{{$disciplina->MediaTurmaGeral()}}</h3>

                                <p>Média anual da sala</p>
                            </div>
                            <div class="icon">
                                <i class="ion ion-stats-bars"></i>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div id="acoes">
                <div class="box-header with-border">
                    <i class="fa fa-eye"></i>

                    <h3 class="box-title">Ações</h3>
                </div>
                <div class="box-body">

                    <div id="digitacao">
                        <div class="box-header with-border">
                            <i class="fa fa-pencil"></i>

                            <h3 class="box-title">Digitação</h3>
                        </div>
                        <div class="box-body">
                            <a href="{{ route('professor.notas.show', $disciplina->id) }}" class="btn btn-primary"
                               style="margin-right: 15px;">Visualizar Nota</a>

                            @for($tipo=1;$tipo<5;$tipo++)
                                @if(Sistema::PermitidoDigitar($disciplina->id,$tipo))
                                    <a href="{{ route('professor.notas.edit', ['disciplina'=>$disciplina->id,'bimestre'=>$tipo]) }}"
                                       class="btn btn-primary" role="button" style="margin-right: 15px;">
                                        Digitar Nota - {{$tipo}}º Bimestre
                                    </a>
                                @endif
                            @endfor

                            @for($tipo=5;$tipo<=7;$tipo++)
                                @if(Sistema::PermitidoDigitar($disciplina->id,$tipo))
                                    @if($tipo==5) @php $a=1; $string="- 1º Semestre" @endphp
                                    @elseif($tipo==6) @php $a=2; $string="- 2º Semestre" @endphp
                                    @else @php $a=3; $string=" Final" @endphp
                                    @endif

                                    <a href="{{ route('professor.recuperacao.edit', ['disciplina'=>$disciplina->id,'recuperacao'=>$a]) }}"
                                       class="btn btn-primary" role="button" style="margin-right: 15px;">
                                        Digitar Recuperação {{$string}}
                                    </a>
                                @endif
                            @endfor

                            @for($tipo=8;$tipo<=11;$tipo++)
                                @if(Sistema::PermitidoDigitar($disciplina->id,$tipo))
                                    <a href="{{ route('fichaavaliacao.index', ['disciplina'=>$disciplina->id,'bimestre'=>$tipo-7]) }}"
                                       class="btn btn-primary" role="button" style="margin-right: 15px;">
                                        Digitar Ficha de Avaliação - {{$tipo-7}}º Bimestre
                                    </a>
                                @endif
                            @endfor
                        </div>
                    </div>

                    <div id="relatorios">
                        <div class="box-header with-border">
                            <i class="fa fa-file"></i>

                            <h3 class="box-title">Relatórios</h3>
                        </div>

                        <div class="box-body" id="professor_relatorios">
                            <button class="btn btn-info" data-toggle="collapse" data-target="#collapse_lista_presenca" data-parent="#professor_relatorios">Lista de presença</button>

                            <a class="btn btn-info" href="{{route('professor.disciplinas.boletim', $disciplina->id) }}" >Notas</a>


                            <div id="collapse_lista_presenca" class="collapse">
                                <br>
                                <form class="form-inline" action="{{route('professor.disciplinas.lista', $disciplina->id) }}">
                                    <input type="text" placeholder="Título (opcional)" class="form-control center-block" name="titulo">
                                    <input type="submit" class="btn btn-secondary">
                                </form>
                            </div>




                        </div>


                    </div>

                </div>



            </div>

        </div>

    </div>
@endsection