@extends('adminlte::page')

@section('title', 'Digitar Ficha de Avaliação')

@section('content_header')
    <h1>
        {{$disciplina->CodigoCti}} - {{ $disciplina->descricao }}
        <small>Digitar Ficha de Avaliação do {{ $bimestre }}º Bimestre</small>
    </h1>
@stop

@section('content')

    <script type="text/javascript">
        function permitir_digitacao($id)
        {
            if(document.getElementById('obs['+$id+']'))
            {
                if(document.getElementById('obs['+$id+']').disabled==false)//Pode Alterar
                    document.getElementById('obs['+$id+']').disabled=true;
                else//Não pode alterar
                    document.getElementById('obs['+$id+']').disabled=false;
            }
        }
    </script>
    <div class="panel">

        <div class="panel-heading">
            <h3 class="panel-title"><strong>Aluno: </strong>{{$aluno->matricula}} - {{$aluno->nome}}</h3>
        </div>

        <div class="panel-body">

            {{ Form::open(array('route' => 'professor.fichaavaliacao.store')) }}

            {{Form::hidden('aluno',$aluno->matricula)}}
            {{Form::hidden('disciplina',$disciplina->id)}}
            {{Form::hidden('bimestre',$bimestre)}}

            @forelse ($fichagrupos as $fgrupo)
                @if(count($fgrupo->itens)>0)
                    <div class="box box-primary">
                        <div class="box-header"><h3 class="box-title">{{ $fgrupo->descricao }}</h3></div>
                        <div class="box-body">

                            @foreach($fgrupo->itens as $item)

                                <div class="form-group">
                                    <div class="checkbox">
                                        <label>
                                            @if($item->tem_texto)
                                                {{Form::checkbox('item['.$item->id.']', null, ($item->PossuiAluno($aluno->matricula, $disciplina->id,$bimestre) == null ? null : true),  ['onclick="permitir_digitacao('.$item->id.')"']) }}
                                            @else
                                                {{Form::checkbox('item['.$item->id.']', null, ($item->PossuiAluno($aluno->matricula, $disciplina->id,$bimestre) == null ? null : true) ) }}
                                            @endif
                                            {{$item->descricao}}
                                        </label>
                                    </div>
                                    @if($item->tem_texto)
                                        @if($item->PossuiAluno($aluno->matricula, $disciplina->id,$bimestre) == null)
                                            <textarea class="form-control" id="obs[{{$item->id}}]" name="obs[{{$item->id}}]" rows="2" disabled></textarea>
                                        @else
                                            <textarea class="form-control" id="obs[{{$item->id}}]" name="obs[{{$item->id}}]" rows="2">{{ $item->PossuiAluno($aluno->matricula, $disciplina->id,$bimestre)->obs }}</textarea>
                                        @endif
                                    @endif
                                </div>
                            @endforeach
                        </div>
                    </div>
                @endif
                @empty
                    Nenhum Grupo cadastrado até o momento
                @endforelse
        </div>

        <div class="panel-footer">
            {{ Form::submit('Salvar Ficha de Avaliação', array('class' => 'btn btn-primary')) }}
            {{ Form::close() }}
        </div>

    </div>

@endsection