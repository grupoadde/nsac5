@extends('adminlte::page')

@section('title', 'Digitar Ficha de Avaliação')

@section('content_header')
    <h1>
        {{$dados['disciplina']->CodigoCti}} - {{ $dados['disciplina']->descricao }}
        <small>Digitar Ficha de Avaliação do {{ $bimestre }}º Bimestre</small>
    </h1>
@stop

@section('content')

    <div class="box box-primary">
        <div class="box-header with-border"><h3 class="box-title">Lista de Alunos</h3></div>

        <div class="box-body">
            <table class="table table-bordered table-hover text-center" id="notas">
                <thead>
                    <tr>
                        <th class="row col-sm-7">
                            <div class="col-sm-1 text-center" data-toggle="tooltip" title="Número">Nº</div>
                            <div class="col-sm-3 text-center" data-toggle="tooltip" title="Registro Acadêmico">
                                R.A.
                            </div>
                            <div class="col-sm-8 text-left" data-toggle="tooltip" title="Nome do aluno(a)">Nome
                            </div>
                        </th>

                        <th class="row col-sm-2 text-center">
                            Ficha de Avaliação
                        </th>

                    </tr>
                </thead>
                <tbody>
                    @foreach($dados['alunos_matriculados'] as $aluno)

                        @if($aluno->dado->notaabaixode($dados['disciplina']->id,6,$bimestre))

                            @if(count($dados['alunos_dispensados'])>0)
                                @foreach($dados['alunos_dispensados'] as $dispensados)
                                    @if($dispensados->matricula == $aluno->dado->matricula)
                                        @php $class = 'danger'; $title = 'Aluno dispensado(a)'; @endphp
                                    @else
                                        @php $class = ''; $title = ''; @endphp
                                    @endif
                                @endforeach
                            @else
                                @php $class=''; $title = ''; @endphp
                            @endif

                            <tr class="{{$class}}" title="{{$title}}">
                                <td class="row" style="vertical-align: middle;">
                                    <div class="col-sm-1" style="vertical-align: middle;">{{$aluno->ordem}}</div>
                                    <div class="col-sm-3" style="vertical-align: middle;">{{$aluno->dado->matricula}}</div>
                                    <div class="col-sm-8 text-left" style="vertical-align: middle;">{{$aluno->dado->nome}}</div>
                                </td>

                                <td class="row" style="vertical-align: middle; padding-left: 7%">

                                        <a href="{{ Route('professor.fichaavaliacao.create',['disciplina'=>$dados['disciplina']->id,'bimestre'=>$bimestre,'aluno'=>$aluno->dado->matricula]) }}" class="btn btn-info pull-left">Digitar</a>

                                        @if(count($aluno->dado->possuiFichaAvaliacao($dados['disciplina']->id,$bimestre))> 0)
                                            <span title="Ficha de Avaliação salva com sucesso" class="glyphicon glyphicon-ok-sign" style="color: green; font-size:2em"></span>
                                        @endif

                                </td>
                            </tr>
                        @endif
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
@endsection