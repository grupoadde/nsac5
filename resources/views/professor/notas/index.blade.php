@extends('adminlte::page')

@section('title', 'NSac')

@section('content_header')
    <h1>Disciplinas
        <small>Digitação de Nota</small>
    </h1>
@stop

@section('content')

    <script language="JavaScript" type="text/JavaScript">
        function toggleBim(bi) {
            if (bi > 4) {
                $('.rec' + (bi - 4)).toggle();
                var colspan = $('.status-diferente').attr('colspan');
                if ($('.bi' + bi).first().is(':visible')) {
                    $('.status-diferente').attr('colspan', Number(colspan) + 1);
                } else {
                    $('.status-diferente').attr('colspan', Number(colspan) - 1);
                }
            } else {

                $('.bi' + bi).toggle();
                var colspan = $('.status-diferente').attr('colspan');
                if ($('.bi' + bi).first().is(':visible')) {
                    $('.status-diferente').attr('colspan', Number(colspan) + 2);
                } else {
                    $('.status-diferente').attr('colspan', Number(colspan) - 2);
                }
            }

        }

		function filtro_todos() {
            $('#notas tbody tr').show();
        }

        function filtro_recuperacao() {
            var nth = ".medfin";
            $("#notas tbody tr").show();
            $(nth).each(function () {
                if ($(this).text() >= 6)
                    $(this).closest("tr").hide();
            });
        }

        function filtro_acima_da_media() {
            var nth = ".medfin";
            $('#notas tbody tr').show();
            $(nth).each(function () {
                if ($(this).text() < 6)
                    $(this).parent().hide();
            });
        }

    </script>
	
	@php 
		$disabled1 = ""; $disabled2 = ""; $disabled3 = ""; $disabled4 = "";
		$toggleBim1 = "toggleBim(1)"; $toggleBim2 = "toggleBim(2)"; $toggleBim3 = "toggleBim(3)"; $toggleBim4 = "toggleBim(4)";
	@endphp

    @if($dados['bimestre'] == 1)
        <script>
            window.onload = function(){
                toggleBim(2);
                toggleBim(3);
                toggleBim(4);
                toggleBim(5);
                toggleBim(6);
            };
						 
        </script>
		@if(Route::currentRouteName() == 'professor.notas.edit')
			@php 
				$toggleBim1="";
				$disabled1 = "disabled"; 
			@endphp
		@endif
    @endif

    @if($dados['bimestre'] == 2)
        <script>
            window.onload = function(){
                toggleBim(3);
                toggleBim(4);
                toggleBim(5);
                toggleBim(6);
						
            };
		</script>

		@if(Route::currentRouteName() == 'professor.notas.edit')
			@php 
				$toggleBim2="";
				$disabled2 = "disabled"; 
			@endphp
		@endif
    @endif

    @if($dados['bimestre'] == 3)
        <script>
            window.onload = function(){
                toggleBim(4);
                toggleBim(5);
                toggleBim(6);
            };
			
        </script>
		@if(Route::currentRouteName() == 'professor.notas.edit')
			@php 
				$toggleBim3="";
				$disabled3 = "disabled"; 
			@endphp
		@endif
    @endif

	@if($dados['bimestre'] == 4)
		@if(Route::currentRouteName() == 'professor.notas.edit')
			@php 
				$toggleBim4="";
				$disabled4 = "disabled"; 
			@endphp
		@endif
	@endif
	
	
    <div class="panel">

        <div class="panel-heading">
            <h3 class="box-title">
                <strong>{{ $dados['disciplina']->CodigoCti }}</strong> - {{ $dados['disciplina']->descricao }}
            </h3>
        </div>

        <div class="panel-body">

            <div class="box box-primary">
                <div class="box-header with-border"><h3 class="box-title">Filtros</h3></div>
                <div class="box-body">
                    <div class=" pull-left col-md-8 col-xs-12"><p>Bimestre</p>
                        <div class="btn-group" data-toggle="buttons">

                            <label class="btn btn-primary" id="btn1bim" onClick="{{$toggleBim1}}" {{$disabled1}}>
                                <input type="checkbox" autocomplete="off" {{$disabled1}}> 1º Bimestre
                            </label>
                            <label class="btn btn-primary" id="btn2bim" onClick="{{$toggleBim2}}" {{$disabled2}}>
                                <input type="checkbox" autocomplete="off" {{$disabled2}}> 2º Bimestre
                            </label>
                            <label class="btn btn-primary" id="btn3bim" onClick="{{$toggleBim3}}" {{$disabled3}}>
                                <input type="checkbox" autocomplete="off" {{$disabled3}}> 3º Bimestre
                            </label>
                            <label class="btn btn-primary" id="btn4bim" onClick="{{$toggleBim4}}" {{$disabled4}}>
                                <input type="checkbox" autocomplete="off" {{$disabled4}}> 4º Bimestre
                            </label>
                            <label class="btn btn-primary" id="btnrec" onClick="toggleBim(5); toggleBim(6);">
                                <input type="checkbox" autocomplete="off"> Recuperações
                            </label>
                        </div>
                    </div>

                    <div class="pull-left col-md-4 col-xs-12"><p><strong>Média Final</strong></p>
                        <div class="btn-group" data-toggle="buttons">
                            <label class="btn btn-info" onClick="filtro_todos()">
                                <input type="radio" name="options" id="todos" autocomplete="off"> Todos
                            </label>
                            <label class="btn btn-success" onClick="filtro_acima_da_media()">
                                <input type="radio" name="options" id="option2" autocomplete="off"> Acima da média
                            </label>
                            <label class="btn btn-danger" onClick="filtro_recuperacao()">
                                <input type="radio" name="options" id="option3" autocomplete="off"> Abaixo da média
                            </label>
                        </div>
                    </div>
                </div>
            </div>

            @if(Route::currentRouteName() == 'professor.notas.show')
                @include('professor.notas.tabela')
            @endif

            @if(Route::currentRouteName() == 'professor.notas.edit')

                <form method="post" id="form-notas" action="{{Route('professor.notas.update', $dados['disciplina']->id)}}">

                    {{ csrf_field() }}
                    {{ Form::hidden('disciplina', $dados['disciplina']->id) }}
                    {{ Form::hidden('bimestre', $dados['bimestre']) }}


                    @include('professor.notas.tabela')


                    <div class="text-center">
                        <button type="submit" class="btn btn-primary btn-md"> Atualizar</button>
                    </div>
					<br>
                </form>
            @endif


        </div>
    </div>
@endsection

@section('js')

    <script src="{{ asset('js/jquery.mask.js') }}" type="text/javascript"></script>
    <script src="{{ asset('js/jquery.formnavigation.js') }}" type="text/javascript"></script>

    <script>
        $(document).ready(function () {
            $('.table-notas').formNavigation();
        });
    </script>

    <script language="JavaScript" type="text/JavaScript">

        $('.nb').blur(function() {
            if(!$.trim(this.value).length) { // zero-length string AFTER a trim
                //alert('a');
                this.value = '0.0';
            }
            if(this.value > 10) {
                this.value = '10.0';
            }
        });
    </script>

@endsection
