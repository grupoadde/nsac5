@if($bimestre > 4)

    @if(($dados['bimestre'] == $bimestre)&&(Route::currentRouteName() == 'professor.notas.edit'))
        {{--DIGITANDO RECUPERAÇÃO--}}
    @endif

    <td class="text-center rec{{ $bimestre-4 }}" style="vertical-align: middle;">

    <span title="{{ App\Models\Nota::REC[$aluno->dado->notadisciplina($dados['disciplina']->id)->$nota] }}">
        @if($aluno->dado->notadisciplina($dados['disciplina']->id)->$nota == 0)
            <span class="label label-warning" data-toggle="tooltip" title="Não Compareceu">NC</span>
        @elseif($aluno->dado->notadisciplina($dados['disciplina']->id)->$nota == 1)
            <span class="label label-primary" data-toggle="tooltip" title="Satisfatório">SAT</span>
        @elseif($aluno->dado->notadisciplina($dados['disciplina']->id)->$nota == 2)
            <span class="label label-danger" data-toggle="tooltip" title="Insatisfatório">INS</span>
        @elseif($aluno->dado->notadisciplina($dados['disciplina']->id)->$nota == 3)
            -
        @endif
    </span>

    </td>

@else

    @if(($dados['bimestre'] == $bimestre)&&(Route::currentRouteName() == 'professor.notas.edit'))
        <td class="" style="vertical-align: middle; background-color: #fafafa;">
            <div title="Nota">
                    <input type="text" class="form-control center-block nb" style="width:50px;" id="nb{{$aluno->dado->matricula}}" name="nb{{$aluno->dado->matricula}}"
                           value="{{Sistema::FormatarNota($aluno->dado->notadisciplina($dados['disciplina']->id)->$nota)}}" data-mask="00.0" data-mask-reverse="true" data-mask-selectonfocus="true">
            </div>
        </td>

        <td style="background-color: #fafafa">
            <div title="Faltas" style="vertical-align: middle;">
                <input type="text" style="width:50px" class="form-control center-block" id="fb{{$aluno->dado->matricula}}"
                       name="fb{{$aluno->dado->matricula}}"
                       value="{{$aluno->dado->notadisciplina($dados['disciplina']->id)->$falta}}" data-mask="00" data-mask-selectonfocus="true" >
            </div>
        </td>

        <td class="text-center" style="vertical-align: middle; background-color: #fafafa">
            <input type="checkbox"   name="nc{{$aluno->dado->matricula}}" id="nc{{$aluno->dado->matricula}}"
                   @if($aluno->dado->notadisciplina($dados['disciplina']->id)->$nc == true)
                   checked
                    @endif
            >
        </td>

    @else
        <td class="text-center bi{{ $bimestre }}" style=" vertical-align: middle;">
            {{ Sistema::FormatarNota($aluno->dado->notadisciplina($dados['disciplina']->id)->$nota) }}
            @if($aluno->dado->notadisciplina($dados['disciplina']->id)->$nc == true)
                <span title="Não compareceu à avaliação bimestral" class="glyphicon glyphicon-exclamation-sign"
                      style="color: orange;"></span>
            @endif
        </td>
        <td class="text-center bi{{ $bimestre }}" style="vertical-align: middle;">
            {{$aluno->dado->notadisciplina($dados['disciplina']->id)->$falta }}
        </td>
    @endif

@endif
