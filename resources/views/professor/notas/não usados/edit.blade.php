@extends('general.layouts.navsidebar')

@section('content')

    <script language="JavaScript" type="text/JavaScript">

        // $(document).ready(function(){$('#todos').button('toggle');});

        function filtro_todos() {$("#notas tbody tr").show();}

        function filtro_recuperacao()
        {
            var nth = "#notas td:nth-child(4) input";
            $("#notas tbody tr").show();
            $(nth).each(function(){
                if($(this).val() >= 6)
                    $(this).parent().parent().hide();
            });
        }

        function filtro_acima_da_media()
        {
            var nth = "#notas td:nth-child(4) input";
            $("#notas tbody tr").show();
            $(nth).each(function(){
                if($(this).val() < 6)
                    $(this).parent().parent().hide();
            });
        }




    </script>

    <h4 class="text-center">
        <strong>{{$infodisciplina[0]->getCodigoCtiAttribute()}}</strong>
        {{$infodisciplina[0]->descricao}}
    </h4>

    <hr>

    <div class="col-sm-12">
        <div class="text-center col-sm-6" style=" float: left;">

            <?php $selected1 = "";  $selected2 = "";  $selected3 = ""; $selected4 = "";?>

            @if($bimestre == 1)
                <?php $selected1 = "selected"; ?>
            @endif

            @if($bimestre == 2)
                <?php $selected2 = "selected"; ?>
            @endif

            @if($bimestre == 3)
                <?php $selected3 = "selected"; ?>
            @endif

            @if($bimestre == 4)
                <?php $selected4 = "selected"; ?>
            @endif

            <div class="col-sm-6 text-right" style="float: left; height: 3.4rem; line-height: 3.4rem;">Selecione o bimestre:</div>

            <div class="col-sm-6" style="float:left; ">
                <form>
                    <select class="form-control">
                        <option {{$selected1}} id="sel1">1º Bimestre</option>
                        <option {{$selected2}} id="sel2">2º Bimestre</option>
                        <option {{$selected3}} id="sel3">3º Bimestre</option>
                        <option {{$selected4}} id="sel4">4º Bimestre</option>
                    </select>

                </form>
            </div>

        </div>
        <div class="text-center col-sm-6" style="float: left;">
            Filtrar por:

            <div class="btn-group" data-toggle="buttons">
                <label class="btn btn-info" onClick="filtro_todos()"
                <input type="radio" name="options" id="todos" autocomplete="off"> Todos
                </label>
                <label class="btn btn-success" onClick="filtro_acima_da_media()">
                    <input type="radio" name="options" id="option2" autocomplete="off"> Acima da média
                </label>
                <label class="btn btn-danger" onClick="filtro_recuperacao()">
                    <input type="radio" name="options" id="option3" autocomplete="off"> Abaixo da média
                </label>
            </div>

        </div>
    </div>
    <br><br>
    <hr>

    <form method="post" action="/notas/update/{{$infodisciplina[0]->id}}">

        {{ csrf_field() }}

        <div class="text-center">
            <button type="submit" class="btn btn-default btn-md"> Atualizar </button>
        </div>
        <hr>


        <div class="wrapper" style="">
            <div class="container-fluid" style="width: 100%;">
                <table class="table table-hover" id="notas">
                    <thead>
                    <tr class="row" style="">
                        <th class="col-sm-1" title="Número">Nº</th>
                        <th class="col-sm-2" title="Registro Acadêmico">R.A.</th>
                        <th class="col-sm-5" title="Nome do aluno(a)">Aluno(a)</th>
                        <th class="col-sm-1" title="Nota">Nota</th>
                        <th class="col-sm-1" title="Faltas">Faltas</th>
                        <th class="col-sm-1" title="Não Compareceu">N.C.</th>
                    </tr>
                    </thead>

                    <tbody>

                    @if($bimestre==1)
                        <input type="hidden" name="bimestre_atual" value="{{$bimestre}}">
                        @foreach($alunos[0]->turmas->alunos as $aluno)
                            @if(!ISSET($aluno->dispensa))
                                <tr class="row" style="">
                                    <td class="col-sm-1" title="Número">{{$loop->index+1}}</td>
                                    <td class="col-sm-2" title="Registro Acadêmico">{{$aluno->matricula}}</td>
                                    <td class="col-sm-5" title="Nome do aluno(a)">{{$aluno->nome}}</td>
                                    <td class="col-sm-1" title="Nota" ><input type="text" id="nb" class="form-control input-md" name="nb{{$aluno->matricula}}" value="{{$aluno->notas[0]->nb1}}" ></td>
                                    <td class="col-sm-1" title="Faltas"><input type="number" id="fb" class="form-control input-md" name="fb{{$aluno->matricula}}"  value="{{$aluno->notas[0]->fb1}}"></td>
                                    <td class="col-sm-1" title="Presença">@if($aluno->notas[0]->nc1 == false) Não @else Sim @endif</td>
                                </tr>
                            @endif

                            @if(ISSET($aluno->dispensa))
                                <tr class="row" style="">

                                    <td class="col-sm-1" title="Número">{{$loop->index+1}}</td>
                                    <td class="col-sm-3" title="Registro Acadêmico">{{$aluno->matricula}}</td>
                                    <td class="col-sm-5" title="Nome do aluno(a)">{{$aluno->nome}}</td>
                                    <td class="col-sm-1" title="Nota">{{$aluno->dispensa['nota']}}</td>
                                    <td class="col-sm-1" title="Faltas">---</td>
                                    <td class="col-sm-1" title="Não Compareceu">---</td>
                                </tr>
                            @endif

                        @endforeach
                    @endif


                    @if($bimestre==2)
                        <input type="hidden" name="bimestre_atual" value="{{$bimestre}}">
                        @foreach($alunos[0]->turmas->alunos as $aluno)
                            <?php $status = ""; ?>
                            @if(!isset($aluno->dispensa))
                                @if($aluno->notas[0]->nb2 < 6 )
                                    <?php $status = "danger"; ?>
                                @else
                                    <?php $status = "success"; ?>
                                @endif
                            @endif

                            @if(!ISSET($aluno->dispensa))
                                <tr class="row {{$status}}" style="">
                                    <td class="col-sm-1" title="Número">{{$loop->index+1}}</td>
                                    <td class="col-sm-2" title="Registro Acadêmico">{{$aluno->matricula}}</td>
                                    <td class="col-sm-5" title="Nome do aluno(a)">{{$aluno->nome}}</td>
                                    <td class="col-sm-1" title="Nota" ><input type="text" id="nb{{$aluno->matricula}}" name="nb{{$aluno->matricula}}" class="form-control input-md" value="{{$aluno->notas[0]->nb2}}" ></td>
                                    <td class="col-sm-1" title="Faltas"><input type="number" id="fb{{$aluno->matricula}}" name="fb{{$aluno->matricula}}" class="form-control input-md"  value="{{$aluno->notas[0]->fb2}}"></td>
                                    <td class="col-sm-1" title="Presença">
                                        @if($aluno->notas[0]->nc2 == false)
                                            <?php $checked = ''; ?>
                                        @else
                                            <?php $checked = 'checked'; ?>
                                        @endif

                                        <input type="checkbox" name="nc{{$aluno->matricula}}" id="nc{{$aluno->matricula}}" {{$checked}}>

                                    </td>

                                </tr>
                            @endif

                            @if(ISSET($aluno->dispensa))
                                <tr class="row" style="">

                                    <td class="col-sm-1" title="Número">{{$loop->index+1}}</td>
                                    <td class="col-sm-3" title="Registro Acadêmico">{{$aluno->matricula}}</td>
                                    <td class="col-sm-5" title="Nome do aluno(a)">{{$aluno->nome}}</td>
                                    <td class="col-sm-1" title="Nota">{{$aluno->dispensa['nota']}}</td>
                                    <td class="col-sm-1" title="Faltas">---</td>
                                    <td class="col-sm-1" title="Não Compareceu">---</td>
                                </tr>
                            @endif

                        @endforeach
                    @endif

                    @if($bimestre==3)
                        <input type="hidden" name="bimestre_atual" value="{{$bimestre}}">
                        @foreach($alunos[0]->turmas->alunos as $aluno)

                            @if(!ISSET($aluno->dispensa))
                                <tr class="row {{$status}}" style="">
                                    <td class="col-sm-1" title="Número">{{$loop->index+1}}</td>
                                    <td class="col-sm-2" title="Registro Acadêmico">{{$aluno->matricula}}</td>
                                    <td class="col-sm-5" title="Nome do aluno(a)">{{$aluno->nome}}</td>
                                    <td class="col-sm-1" title="Nota" ><input type="text" id="nb{{$aluno->matricula}}" name="nb{{$aluno->matricula}}" class="form-control input-md" value="{{$aluno->notas[0]->nb3}}" ></td>
                                    <td class="col-sm-1" title="Faltas"><input type="number" id="fb{{$aluno->matricula}}" name="fb{{$aluno->matricula}}" class="form-control input-md"  value="{{$aluno->notas[0]->fb3}}"></td>
                                    <td class="col-sm-1" title="Presença">
                                        @if($aluno->notas[0]->nc3 == false)
                                            <?php $checked = ''; ?>
                                        @else
                                            <?php $checked = 'checked'; ?>
                                        @endif

                                        <input type="checkbox" name="nc{{$aluno->matricula}}" id="nc{{$aluno->matricula}}" {{$checked}}>

                                    </td>

                                </tr>
                            @endif

                            @if(ISSET($aluno->dispensa))
                                <tr class="row" style="">

                                    <td class="col-sm-1" title="Número">{{$loop->index+1}}</td>
                                    <td class="col-sm-3" title="Registro Acadêmico">{{$aluno->matricula}}</td>
                                    <td class="col-sm-5" title="Nome do aluno(a)">{{$aluno->nome}}</td>
                                    <td class="col-sm-1" title="Nota">{{$aluno->dispensa['nota']}}</td>
                                    <td class="col-sm-1" title="Faltas">---</td>
                                    <td class="col-sm-1" title="Não Compareceu">---</td>
                                </tr>
                            @endif

                        @endforeach
                    @endif

                    @if($bimestre==4)
                        <input type="hidden" name="bimestre_atual" value="{{$bimestre}}">
                        @foreach($alunos[0]->turmas->alunos as $aluno)
                            <?php $status = ""; ?>

                            @if(!ISSET($aluno->dispensa))
                                <tr class="row {{$status}}" style="">
                                    <td class="col-sm-1" title="Número">{{$loop->index+1}}</td>
                                    <td class="col-sm-2" title="Registro Acadêmico">{{$aluno->matricula}}</td>
                                    <td class="col-sm-5" title="Nome do aluno(a)">{{$aluno->nome}}</td>
                                    <td class="col-sm-1" title="Nota" ><input type="text" id="nb{{$aluno->matricula}}" name="nb{{$aluno->matricula}}" class="form-control input-md" value="{{$aluno->notas[0]->nb4}}" ></td>
                                    <td class="col-sm-1" title="Faltas"><input type="number" id="fb{{$aluno->matricula}}" name="fb{{$aluno->matricula}}" class="form-control input-md"  value="{{$aluno->notas[0]->fb4}}"></td>
                                    <td class="col-sm-1" title="Presença">
                                        @if($aluno->notas[0]->nc4 == false)
                                            <?php $checked = ''; ?>
                                        @else
                                            <?php $checked = 'checked'; ?>
                                        @endif

                                        <input type="checkbox" name="nc{{$aluno->matricula}}" id="nc{{$aluno->matricula}}" {{$checked}}>

                                    </td>

                                </tr>
                            @endif

                            @if(ISSET($aluno->dispensa))
                                <tr class="row" style="">

                                    <td class="col-sm-1" title="Número">{{$loop->index+1}}</td>
                                    <td class="col-sm-3" title="Registro Acadêmico">{{$aluno->matricula}}</td>
                                    <td class="col-sm-5" title="Nome do aluno(a)">{{$aluno->nome}}</td>
                                    <td class="col-sm-1" title="Nota">{{$aluno->dispensa['nota']}}</td>
                                    <td class="col-sm-1" title="Faltas">---</td>
                                    <td class="col-sm-1" title="Não Compareceu">---</td>
                                </tr>
                            @endif

                        @endforeach
                    @endif

                    </tbody>

                </table>
            </div>
        </div>

    </form>






@endsection