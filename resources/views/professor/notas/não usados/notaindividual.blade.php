
@if($bimestre > 4)

    @if(($dados['bimestre'] == $bimestre)&&(Route::currentRouteName() == 'professor.notas.edit'))
        {{--DIGITANDO RECUPERAÇÃO--}}
    @endif

    <td class="row col-xs-1 text-center" style="vertical-align: middle;">

    <span title="{{ App\Models\Nota::REC[$aluno->dado->notadisciplina($dados['disciplina']->id)->$nota] }}" >
        @if($aluno->dado->notadisciplina($dados['disciplina']->id)->$nota == 0)
            {{Html::image('imgs/recuperacao/nc.png')}}
        @elseif($aluno->dado->notadisciplina($dados['disciplina']->id)->$nota == 1)
            {{Html::image('imgs/recuperacao/sat.png')}}
        @elseif($aluno->dado->notadisciplina($dados['disciplina']->id)->$nota == 2)
            {{Html::image('imgs/recuperacao/ins.png')}}
        @elseif($aluno->dado->notadisciplina($dados['disciplina']->id)->$nota == 3)
            {{--{{Html::image('imgs/recuperacao/npr.png')}}--}}-
        @endif
    </span>

@else

    @if(($dados['bimestre'] == $bimestre)&&(Route::currentRouteName() == 'professor.notas.edit'))
        <td class="row col-sm-2 text-center" style="vertical-align: middle;">

            <div class="col-sm-5" title="Nota">
                <input type="text" class="form-control" style="width:100%" id="nb" name="nb{{$aluno->dado->matricula}}" value="{{$aluno->dado->notadisciplina($dados['disciplina']->id)->$nota}}" style="min-width: 50%;">
            </div>
            <div class="col-sm-5" title="Faltas">
                <input type="number" style="width:100%" class="form-control" id="fb" name="fb{{$aluno->dado->matricula}}"  value="{{$aluno->dado->notadisciplina($dados['disciplina']->id)->$falta}}">
            </div>
            <div class="checkbox col-sm-1">
                <input type="checkbox" style="width:75%" name="nc{{$aluno->dado->matricula}}" id="nc{{$aluno->dado->matricula}}"
                       @if($aluno->dado->notadisciplina($dados['disciplina']->id)->$nc == true)
                       checked
                        @endif
                ></div>

    @else
        <td class="row col-sm-1 text-center" style="vertical-align: middle;">
        <div class="col-sm-6" style="white-space: nowrap;">
            {{ $aluno->dado->notadisciplina($dados['disciplina']->id)->$nota }}
            @if($aluno->dado->notadisciplina($dados['disciplina']->id)->$nc == true)
                <span title="Não compareceu à prova" class="glyphicon glyphicon-exclamation-sign" style="color: orange;"></span>
            @endif
        </div>
        <div class="col-sm-6">
            {{$aluno->dado->notadisciplina($dados['disciplina']->id)->$falta }}
        </div>
    @endif

@endif
