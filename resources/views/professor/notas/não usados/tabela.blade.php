<div class="box box-primary">
    <div class="box-header with-border"><h3 class="box-title">Lista de Alunos</h3></div>
    <div class="box-body">

        <table class="table table-bordered table-hover text-center" id="notas">
            <thead>
            <tr>
                <th class="col-sm-4 text-center" title="Dados do aluno(a)"> ALUNO(A)</th>
                <th class="col-sm-1 text-center" title="Primeiro bimestre">1&ordm; BIMESTRE <br>
                    <small>Média: {{$dados['disciplina']->MediaTurma(1)}}</small>
                </th>
                <th class="col-sm-1 text-center" title="Segundo bimestre">2&ordm; BIMESTRE <br>
                    <small>Média: {{$dados['disciplina']->MediaTurma(2)}}</small>
                </th>
                <th class="col-sm-1"></th>
                <th class="col-sm-1 text-center" title="Terceiro bimestre">3&ordm; BIMESTRE <br>
                    <small>Média: {{$dados['disciplina']->MediaTurma(3)}}</small>
                </th>
                <th class="col-sm-1 text-center" title="Quarto bimestre">4&ordm; BIMESTRE <br>
                    <small>Média: {{$dados['disciplina']->MediaTurma(4)}}</small>
                </th>
                <th class="col-sm-1"></th>
                <th class="col-sm-1"></th>
            </tr>
            <tr>
                <th class="row col-sm-4">
                    <div class="col-sm-1 text-center" data-toggle="tooltip" title="Número">Nº</div>
                    <div class="col-sm-3 hidden-xs text-center" data-toggle="tooltip" title="Registro Acadêmico">
                        R.A.
                    </div>
                    <div class="col-sm-8 text-left" data-toggle="tooltip" title="Nome do aluno(a)">Nome
                    </div>
                </th>
                <th class="row col-sm-1 text-center">
                    <div class="col-sm-6" data-toggle="tooltip" title="Média Bimestral">M.B.</div>
                    <div class="col-sm-6" data-toggle="tooltip" title="Faltas">F.</div>
                </th>
                <th class="row col-sm-1 text-center">
                    <div class="col-sm-5" data-toggle="tooltip" title="Média Bimestral">M.B.</div>
                    <div class="col-sm-5" data-toggle="tooltip" title="Faltas">F.</div>
                    <div class="col-sm-2" data-toggle="tooltip"></div>
                </th>
                <th class="row col-sm-1 text-center">
                    <div class="col-sm-12" data-toggle="tooltip" title="Recuperação 1º Semestre">REC.1</div>
                 </th>
                <th class="row col-sm-1 text-center">
                    <div class="col-sm-6" data-toggle="tooltip" title="Média Bimestral">M.B.</div>
                    <div class="col-sm-6" data-toggle="tooltip" title="Faltas">F.</div>
                </th>
                <th class="row col-sm-1 text-center">
                    <div class="col-sm-6" data-toggle="tooltip" title="Média Bimestral">M.B.</div>
                    <div class="col-sm-6" data-toggle="tooltip" title="Faltas">F.</div>
                </th>
                <th class="row col-sm-1 text-center">
                    <div class="col-sm-12" data-toggle="tooltip" title="Recuperação 2º Semestre">REC.2</div>
                </th>
                <th class="row col-sm-1 text-center">
                    <div class="col-md-12" data-toggle="tooltip" title="Média Final">M.F.</div>
                </th>
            </tr>
            </thead>
            <tbody>
            @foreach($dados['alunos_matriculados'] as $aluno)

                <td class="row col-sm-4 text-center" style="vertical-align: middle;">
                    <div class="col-sm-1">{{$aluno->ordem}}</div>
                    <div class="col-sm-3 hidden-xs">{{$aluno->dado->matricula}}</div>
                    <div class="col-sm-8 text-left">{{$aluno->dado->nome}}</div>
                </td>

                @isset($dispensa)
                    <td class="row col-sm-7 text-center" colspan="6" style="vertical-align: middle;">Aluno Dispensado</td>
                    <td class="row col-sm-1" style="vertical-align: middle;"> {{ $dispensa->nota }}</td>
                @endisset

                @empty($dispensa)

                    @include('professor.notas.não usados.notaindividual', ['bimestre' => 1, 'nota' => 'nb1', 'falta' => 'fb1', 'nc' => 'nc1'])
                    @include('professor.notas.não usados.notaindividual', ['bimestre' => 2, 'nota' => 'nb2', 'falta' => 'fb2', 'nc' => 'nc2'])
                    @include('professor.notas.não usados.notaindividual', ['bimestre' => 5, 'nota' => 'rec1', 'falta' => '', 'nc' => ''])
                    @include('professor.notas.não usados.notaindividual', ['bimestre' => 3, 'nota' => 'nb3', 'falta' => 'fb3', 'nc' => 'nc3'])
                    @include('professor.notas.não usados.notaindividual', ['bimestre' => 4, 'nota' => 'nb4', 'falta' => 'fb4', 'nc' => 'nc4'])
                    @include('professor.notas.não usados.notaindividual', ['bimestre' => 6, 'nota' => 'rec2', 'falta' => '', 'nc' => ''])

                    <td class="row col-sm-1 text-center" style="vertical-align: middle;">
                        {{$aluno->dado->notadisciplina($dados['disciplina']->id)->notafinal}}
                    </td>
                @endempty

                </tr>

            @endforeach
            </tbody>
        </table>
    </div>
</div>
