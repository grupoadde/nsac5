
<div class="box box-primary">
    <div class="box-body box-profile">
        <img class="profile-user-img img-responsive img-circle"
             src="http://img.freepik.com/icones-gratis/forma-usuario_318-42150.jpg?size=338&ext=jpg"
             alt="User profile picture">


        <h3 class="profile-username text-center">{{$prof->nome}}</h3>

        <!--<p class="text-muted text-center">Software Engineer</p>-->


        @if(Route::currentRouteName() != 'professor.perfil')
            <a href="javascript:history.back()" class="btn btn-secondary btn-block"><b>Voltar</b></a>
        @endif

        @if(Route::currentRouteName() != 'professor.perfil.edit.dados')
            <a href="/professor/perfil/edit/dados" class="btn btn-primary btn-block"><b>Editar dados pessoais</b></a>
        @endif

        @if(Route::currentRouteName() != 'professor.perfil.edit.login')
            <a href="/professor/perfil/edit/login" class="btn btn-primary btn-block"><b>Alterar senha</b></a>

        @endif


        @if(Route::currentRouteName() != 'professor.perfil.config')
            <a href="/professor/perfil/config" class="btn btn-primary btn-block"><b>Configurações</b></a>

        @endif



    </div>

</div>