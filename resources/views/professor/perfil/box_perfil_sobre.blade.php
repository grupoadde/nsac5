<div class="box box-primary">
    <div class="box-header with-border">
        <h3 class="box-title">Sobre mim</h3>
    </div>
    <!-- /.box-header -->
    <div class="box-body">
        <strong><i class="fa fa-book margin-r-5"></i>Educação</strong>

        <p class="text-muted">
           -
        </p>

        <hr>

        <strong><i class="fa fa-map-marker margin-r-5"></i> Localização</strong>

        <p class="text-muted">-</p>

        <hr>

        <strong><i class="fa fa-pencil margin-r-5"></i> Habilidades</strong>

        <p>
            <span class="label label-danger">1</span>
            <span class="label label-success">2</span>
            <span class="label label-info">3</span>
            <span class="label label-warning">4</span>
            <span class="label label-primary">5</span>
        </p>

        <hr>

        <strong><i class="fa fa-file-text-o margin-r-5"></i> Notas</strong>
        <p>-</p>
    </div>
    <!-- /.box-body -->
</div>