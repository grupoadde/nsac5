    <div class="box box-default">
        <div class="box-header with-border">
            <h3 class="box-title">Configurações de Digitação</h3>
        </div>
        <div class="box-body">
            <form>
                <div class="form-group col-md-6">
                    <label>Campos de Digitação de Nota</label>
                    <select class="form-control" name="pontoouvirgula" id="pontoouvirgula">
                        <option value="0"@if($prof->config_pontoouvirgula == 0) selected @endif>Utilizar Vírgulas</option>
                            <option value="1"@if($prof->config_pontoouvirgula == 1) selected @endif>Utilizar Pontos</option>
                    </select></div>
                <div class="col-md-6"></div>
            </form>
        </div>
        <div class="box-footer text-center">
            <button class="btn btn-info">Atualizar</button>
        </div>

    </div>

