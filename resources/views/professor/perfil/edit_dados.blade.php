<div class="box box-default">

    <div class="box-header with-border">
        <i class="fa fa-folder-o"></i>

        <h3 class="box-title">Editar dados pessoais</h3>
    </div>

    <div class="box-body">
        <form class="form-horizontal" action="/professor/perfil/edit/dados/store" method="post">
{{csrf_field()}}
            <div class="box-body">
                <!--Nome-->
                <div class="form-group">
                    <label for="nome" class="col-sm-2 control-label">Nome</label>

                    <div class="col-sm-10">
                        <input type="text" class="form-control" name="nome" id="nome" placeholder="Nome" value="{{$prof->nome}}">
                    </div>
                </div>
                <!--Nome-->
                <div class="form-group">
                    <label for="apelido" class="col-sm-2 control-label">Apelido</label>

                    <div class="col-sm-10">
                        <input type="text" class="form-control" name="apelido" id="apelido" placeholder="Nome" value="{{$prof->apelido}}">
                    </div>
                </div>
                <!--E-mail-->
                <div class="form-group">
                    <label for="email" class="col-sm-2 control-label">E-mail</label>

                    <div class="col-sm-10">
                        <input type="email" class="form-control" name="email" id="email" placeholder="E-mail" value="{{$prof->email}}" >
                    </div>
                </div>
                <!--RG-->
                <div class="form-group">
                    <label for="rg" class="col-sm-2 control-label">R.G.</label>

                    <div class="col-sm-10">
                        <input type="text" class="form-control" name="rg" id="rg" placeholder="" value="{{$prof->rg}}" data-mask="99.999.999-#/AA" >
                    </div>
                </div>
                <!--CPF-->
                <div class="form-group">
                    <label for="cpf" class="col-sm-2 control-label">C.P.F.</label>

                    <div class="col-sm-10">
                        <input type="text" class="form-control" name="cpf" id="cpf" placeholder="" value="{{$prof->cpf}}" data-mask="999.999.999-99" >
                    </div>
                </div>
                <!--TELEFONES-->
                <div class="form-group">
                    <label for="telefone" class="col-sm-2 control-label">Telefone</label>

                    <div class="col-sm-10">
                        <input type="text" class="form-control" name="telefone" id="telefone" placeholder="Telefone" value="@if(isset($prof->telefones[0]->numero)){{$prof->telefones[0]->numero}}@endif"   >
                    </div>
                </div>
                <!--FOTO-->
                <div class="form-group">
                    <label for="foto" class="col-sm-2 control-label">Foto</label>

                    <div class="col-sm-10">
                        <input type="file" class="form-control" name="foto" placeholder="">
                    </div>
                </div>

            </div>
            <!-- /.box-body -->
            <div class="box-footer text-center">
                <input type="submit" class="btn btn-info">
            </div>

        </form>


    </div>

</div>