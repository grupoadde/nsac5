<div class="box box-default">

    <div class="box-header with-border">
        <i class="fa fa-folder-o"></i>

        <h3 class="box-title">Alterar senha</h3>
    </div>

    <div class="box-body">
        <form class="form-horizontal" action="/professor/perfil/edit/login/store" method="post">
            {{csrf_field()}}
            <div class="box-body">
                <!--Senha atual-->
                <div class="form-group">
                    <label for="atual" class="col-sm-2 control-label">Senha atual</label>

                    <div class="col-sm-10">
                        <input required type="password" class="form-control" name="atual" id="atual" placeholder="Senha atual" value="">
                    </div>
                </div>
                <!--Senha nova-->
                <div class="form-group">
                    <label for="nova" class="col-sm-2 control-label">Nova senha</label>

                    <div class="col-sm-10">
                        <input required type="password" class="form-control" name="nova" id="nova" placeholder="Nova senha" value="">
                    </div>
                </div>
                <!--Confirmação senha nova-->
                <div class="form-group">
                    <label for="confirmacao" class="col-sm-2 control-label">Confirmar nova senha</label>

                    <div class="col-sm-10">
                        <input required type="password" class="form-control" name="confirmacao" id="confirmacao" placeholder="Confirmação" value="">
                    </div>
                </div>

            </div>
            <!-- /.box-body -->
            <div class="box-footer text-center">
                <button type="submit" class="btn btn-info">Atualizar</button>
            </div>

        </form>


    </div>

</div>