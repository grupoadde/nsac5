@extends('adminlte::page')

@section('title', 'Professor')

@section('content_header')
    <h1>Perfil do Professor
        <small>Dados pessoais e do sistema</small>
    </h1>
@stop

@section('content')
    <section class="content">
        <div class="row">
            <div class="col-md-3">
                @include('professor.perfil.box_perfil')
                {{--@if(Route::currentRouteName() == 'professor.perfil')
                    @include('professor.perfil.box_perfil_sobre');
                @endif--}}
            </div>
            <div class="col-md-9">
                @if(Route::currentRouteName() == 'professor.perfil')
                    @include('professor.perfil.show')
                @endif

                @if(Route::currentRouteName() == 'professor.perfil.edit.dados')
                    @include('professor.perfil.edit_dados')
                @endif

                @if(Route::currentRouteName() == 'professor.perfil.edit.login')
                    @include('professor.perfil.edit_login')
                @endif

                @if(Route::currentRouteName() == 'professor.perfil.config')
                    @include('professor.perfil.config')
                @endif
            </div>
        </div>


        <script>
            window.onload = function()
            {
                var modalalert = "{{ session()->pull('modalalert') }}";
                if(typeof modalalert !== 'undefined' && modalalert)
                {
                    $('#modalalert').modal('show');
                }
            };
        </script>

        @include('modal', ['texto' =>  session()->pull('mensagem')])


    </section>
@endsection