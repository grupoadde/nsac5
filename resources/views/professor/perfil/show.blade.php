<div class="box box-default">

    <div class="box-header with-border">
        <i class="fa fa-folder-o"></i>

        <h3 class="box-title">Dados</h3>
    </div>

    <div class="box-body">

        <dl class="dl-horizontal" style="">
            <dt title="Nome completo">Nome</dt>
            <dd>{{$prof->nome}}</dd>
            <dt title="Apelido">Apelido</dt>
            <dd>{{$prof->apelido}}</dd>
            <dt title="Endereço eletrônico">E-mail</dt>
            <dd>{{$prof->email}}</dd>
            <dt title="Registro Geral">R.G.</dt>
            <dd>{{$prof->rg}}</dd>
            <dt title="Cadastro de Pessoas Físicas">C.P.F.</dt>
            <dd>{{$prof->CpfFormatado}}</dd>
            <dt title="Registro Docente">R.D.</dt>
            <dd>{{$prof->rd}}</dd>
            <dt title="Telefone">Telefone</dt>
            <dd>@if(isset($prof->telefones[0]->numero)){{$prof->telefones[0]->numero}} @else - @endif</dd>
        </dl>
    </div>

</div>

<div class="box box-default">

    <div class="box-header with-border">
        <div class="col-sm-9">
        <i class="fa fa-globe" style="margin-left: -7px; margin-right: 7px;"></i>

        <h3 class="box-title">Atribuição</h3>
        </div>
        <div class="col-sm-3 ">
            {{Sistema::FormatarData($atribuicao[0]->data_inicial)}} a
            {{Sistema::FormatarData($atribuicao[0]->data_final)}}
            {{--$atribuicao[0]->data_inicial}} a {{$atribuicao[0]->data_final--}}
        </div>
    </div>

    <div class="box-body">
        <dl class="dl-horizontal" style="">
            @foreach($atribuicao[0]->disciplinas as $disciplina)
                <dt title="Código">{{$disciplina->CodigoCti}}</dt>
                <dd title="Nome">{{$disciplina->descricao}}</dd>
            @endforeach
        </dl>
    </div>

</div>
