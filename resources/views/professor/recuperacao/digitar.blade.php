@extends('adminlte::page')

@section('title', 'Disciplinas')

@section('content_header')
    <h1>Disciplinas <small>Digitação de Recuperação</small></h1>
@stop

@section('content')

    <div class="panel">

        <div class="panel-heading">
            <h3 class="box-title">
                <strong>{{$dados['disciplina']->CodigoCti}}</strong> -
                {{ $dados['disciplina']->descricao }}<small> - Digitar Recuperação
                @if($dados['recuperacao'] == 1) do 1º Semestre
                @elseif($dados['recuperacao'] == 2) do 2º Semestre
                @elseif($dados['recuperacao'] == 3) Final
                @endif</small>
            </h3>
         </div>

        <div class="panel-body">

            <div class="box box-primary">
                <div class="box-header with-border"><h3 class="box-title">Lista de Alunos</h3></div>
                <div class="box-body">

                <form method="post" action="{{Route('professor.notas.update')}}">
                    {{ csrf_field() }}
                    {{ Form::hidden('disciplina', $dados['disciplina']->id) }}
                    {{ Form::hidden('bimestre', $dados['recuperacao']+4) }}

                    <table class="table table-bordered table-hover text-center table-sm" id="notas">
                        <thead>
                        <tr>
                            <th class="row col-sm-7">
                                <div class="col-sm-1 text-center" data-toggle="tooltip" title="Número">Nº</div>
                                <div class="col-sm-3 text-center" data-toggle="tooltip" title="Registro Acadêmico">
                                    R.A.
                                </div>
                                <div class="col-sm-8 text-left" data-toggle="tooltip" title="Nome do aluno(a)">Nome
                                </div>
                            </th>

                            <th class="row col-sm-5 text-center">
                                Resultado da Recuperação
                            </th>

                        </tr>
                        </thead>
                        <tbody>

                            @php
                                $rec = 'rec'.$dados['recuperacao'];
                                if($rec == 'rec3') $rec = 'recfinal';
                            @endphp
                            @foreach($dados['alunos_matriculados'] as $aluno)

                                @if($aluno->dado->estaderecuperacao($dados['disciplina']->id,$dados['recuperacao']))

                                    <tr
                                            @php $dispensa = $aluno->dado->dispensado($dados['disciplina']->id) @endphp
                                            @isset($dispensa)
                                                class="table-info" title="Alun{{$aluno->dado->sexoF}} Dispensad{{$aluno->dado->sexoF}}"
                                            @endisset
                                            @empty($dispensa)
                                                @if($aluno->situacao > 0)
                                                    class="table-info" title="Alun{{$aluno->dado->sexoF}} {{ $aluno->SituacaoReadable }}"
                                                @endif
                                            @endempty
                                    >

                                        <td class="row" style="vertical-align: middle;">
                                            <div class="col-sm-1" style="vertical-align: middle;">{{$aluno->ordem}}</div>
                                            <div class="col-sm-3" style="vertical-align: middle;">{{$aluno->dado->matricula}}</div>
                                            <div class="col-sm-8 text-left" style="vertical-align: middle;">{{$aluno->dado->nome}}</div>
                                        </td>

                                        @isset($dispensa)

                                            <td class="row text-center" style="vertical-align: middle;">
                                            Alun{{$aluno->dado->sexoF}} Dispensad{{$aluno->dado->sexoF}}
                                            </td>
                                        @endisset

                                        @empty($dispensa)

                                            @if($aluno->situacao > 0)
                                                <td class="row text-center table-info" style="vertical-align: middle;">
                                                Alun{{$aluno->dado->sexoF}} {{ $aluno->SituacaoReadable }}
                                            @else
                                                <td class="row text-center" style="vertical-align: middle;">
                                                    <select class="form-control" name="rc{{$aluno->dado->matricula}}" id="rc{{$aluno->dado->matricula}}">
                                                        <option value="0"@if($aluno->dado->notadisciplina($dados['disciplina']->id)->$rec == 0) selected @endif>Não Compareceu</option>
                                                        <option value="1"@if($aluno->dado->notadisciplina($dados['disciplina']->id)->$rec == 1) selected @endif>Satisfatório</option>
                                                        <option value="2"@if($aluno->dado->notadisciplina($dados['disciplina']->id)->$rec == 2) selected @endif>Insatisfatório</option>
                                                    </select>
                                                @endif
                                            </td>
                                        @endempty
                                    </tr>
                                @endif
                            @endforeach
                            </tbody>
                        </table>
                        <input type="submit" id="btnform" style="display: none;">
                </form>
                </div>
                <div class="box-footer">
                    <label for="btnform" role="button" class="btn btn-primary" tabindex="0">
                        Salvar
                    </label>
                </div>
                </div>

            </div>

    </div>

@endsection