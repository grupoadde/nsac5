@extends('adminlte::page')

@section('title', 'Nova Solicitação de Digitação')

@section('content_header')
    <h1>Nova Solicitação de Digitação</h1>
@stop

@section('content')

    <div class="panel">

        <div class="panel-body">
            {{ Form::open(array('route' => 'professor.solicitacaodigitacao.store')) }}

            <div class="form-group">
                {{ Form::label('disciplina', 'Disciplina') }} &nbsp;
                <select name="disciplina" id="disciplina">
                    @foreach($disciplinas->disciplinas as $disciplina)
                        <option value="{{$disciplina->id}}">{{$disciplina->codigoCTI}} - {{$disciplina->descricao}}</option>
                    @endforeach
                </select>
            </div>

            <div class="form-group">
                {{ Form::label('tipo', 'Tipo') }} &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;

                <select name="tipo" id="tipo">
                    @for($a=1;$a<=11;$a++)
                        <option value="{{$a}}">
                            @if($a<=4)
                                Nota - {{$a}}º Bimestre
                            @elseif($a==5)
                                Recuperação - 1º Semestre
                            @elseif($a==6)
                                Recuperação - 2º Semestre
                            @elseif($a==7)
                                Recuperação Final
                            @elseif($a>7)
                                Ficha de Avaliação - {{$a-7}}º Bimestre
                            @endif
                        </option>
                    @endfor
                </select>
            </div>

            <div class="form-group">
                {{ Form::label('justificativa', 'Justificativa') }}
                {{ Form::textarea('justificativa', null, array('class' => 'form-control','required'=>'required')) }}
            </div>

        </div>
        <div class="panel-footer">
            {{ Form::submit('Enviar Solicitação de Digitação', array('class' => 'btn btn-primary')) }}
            {{ Form::close() }}
        </div>

    </div>

@endsection