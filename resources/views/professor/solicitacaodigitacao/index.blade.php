@extends('adminlte::page')

@section('title', 'Solicitação de Digitação')

@section('content_header')
    <h1>Solicitação de Digitação <small>Digitar notas fora do prazo</small></h1>
@stop

@section('content')
    <div class="box box-primary">
        <div class="box-header with-border">
            <h3 class="box-title">Lista de Solicitações</h3>
        </div>
        <div class="box-body">
            <table class="table table-bordered table-striped">
                <thead>
                    <tr>
                        <th class="text-center">Disciplina</th>
                        <th class="text-center">Tipo</th>
                        <th class="text-center">Situação</th>
                        <th class="text-center">Data Limite</th>
                        <th class="text-center">Digitação</th>
                    </tr>
                </thead>

                <tbody>
                    @forelse ($solicitacoes_digitacao as $solicitacao)
                        <tr>
                            <td class="text-center">
                                {{ $solicitacao->disciplinas->getCodigoCtiAttribute() }} - {{$solicitacao->disciplinas->descricao}}
                            </td>

                            <td class="text-center">
                                @for($tipo=1;$tipo<5;$tipo++)
                                    @if($solicitacao->tipo==$tipo)
                                         @php $rota="professor.notas.edit";$parametros=$tipo;@endphp
                                    @endif
                                @endfor

                                @for($tipo=5;$tipo<=7;$tipo++)
                                        @if($solicitacao->tipo==$tipo)
                                            @if($tipo==5) @php  $a=1; @endphp
                                                @elseif($tipo==6) @php $a=2; @endphp
                                                @else @php $a=3; @endphp
                                            @endif
                                            @php $rota="professor.recuperacao.edit";$parametros=$a;@endphp
                                        @endif
                                @endfor

                                @for($tipo=8;$tipo<=11;$tipo++)

                                   @if($solicitacao->tipo==$tipo)
                                       @php $rota="professor.fichaavaliacao.index"; $parametros=$tipo-7;@endphp
                                   @endif
                                @endfor

                                {{$solicitacao->TipoFormatado}}
                            </td>

                            <!-- Formatação Situação-->
                                <!-- Aguardando Aprovação-->
                                @if($solicitacao->situacao==1)
                                    @php $situacao="disabled";$btncancelar=" ";$data_limite=" - " @endphp
                                <!-- Aprovado-->
                                @elseif($solicitacao->situacao==2)
                                    @php $situacao=" ";$btncancelar=" ";$data_limite=date('d/m/Y',strtotime($solicitacao->data_limite)); @endphp
                                <!-- Digitando-->
                                @elseif($solicitacao->situacao==3)
                                    @php $situacao=" ";$btncancelar="disabled";$data_limite=date('d/m/Y',strtotime($solicitacao->data_limite)); @endphp
                                <!-- Encerrado-->
                                @else
                                    @php $situacao="disabled";$btncancelar="disabled";$data_limite=date('d/m/Y',strtotime($solicitacao->data_limite)); @endphp
                                @endif
                             <!-- Fim Formatação Situação-->

                            <td class="text-center" style="text-align: center">
                                {{$solicitacao->SituacaoFormatada}}
                            </td>
                            <td class="text-center" style="text-align: center">
                                {{$data_limite}}
                            </td>

                            <td style="margin-left: 5%;text-align: center">

                                @if($solicitacao->tipo>=5&&$solicitacao->tipo<=7)
                                    @php $campo="recuperacao" @endphp
                                 @else
                                    @php $campo="bimestre" @endphp
                                 @endif

                                <form action="{{ route('professor.solicitacaodigitacao.show')}}" method="post">
                                    {{ csrf_field() }}
                                    <input type="hidden" name="rota" value="{{$rota}}">
                                    <input type="hidden" name="solicitacao" value="{{$solicitacao->id}}">
                                    <input type="hidden" name="disciplina" value="{{$solicitacao->disciplinas->id}}">
                                    <input type="hidden" name="{{$campo}}" value="{{$parametros}}">
                                    <input type="submit" value="Digitar" class="btn btn-info" style="margin-right: 5px;float: left" {{$situacao}}>
                                </form>

                                {{Form::open(array('route' => array('professor.solicitacaodigitacao.destroy', $solicitacao->id),'method'=>'DELETE'))}}
                                    {{Form::submit('Cancelar', array('class' => 'btn btn-danger',$btncancelar))}}
                                {{Form::close()}}
                            </td>
                        </tr>
                    @empty
                        <td class="text-center" colspan="5">Nenhuma solicitação de digitação cadastrada até o momento</td>
                    @endforelse
                </tbody>
            </table>
        </div>
        <div class="box-footer">
            <a href="{{ Route('professor.solicitacaodigitacao.create') }}" class="btn btn-primary pull-left">Nova Solicitação</a>
        </div>
    </div>

@endsection