<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::group(['namespace' => 'Auth\\'], function() {
    Route::get('login', 'LoginController@showLoginForm')->name('login');
    Route::post('login', 'LoginController@login');
    Route::post('logout', 'LoginController@logout');
});

Route::group([
    'prefix' => 'admin',
    'as' => 'admin.',
    'namespace' => 'Admin\\',
    //'middleware' => ['auth', 'admin'],
], function() {

    Route::resource('users', 'UserController');
    Route::resource('roles', 'RoleController');
    Route::resource('permissions', 'PermissionController');
    Route::resource('professores', 'ProfessorController');

    Route::resource('fichagrupos', 'FichasAvaliacao\FichasGrupoController');

    Route::resource('fichaitens', 'FichasAvaliacao\FichasItemController', ['except' => ['create', 'destroy']]);
    Route::group([
        'prefix' => 'fichaitens',
        'as' => 'fichaitens.',
        'namespace' => 'FichasAvaliacao',
    ], function () {
        Route::get('create/{id}', 'FichasItemController@create')->name('create');
        Route::get('delete/{id}', 'FichasItemController@destroy')->name('destroy');
    });

});

Route::group([
    'prefix' => 'coordenador',
    'as' => 'coordenador.',
    'namespace' => 'Coordenador\\',
    'middleware' => ['auth', 'coordenador'],
], function() {


});

Route::group([
    'prefix' => 'professor',
    'as' => 'professor.',
    'namespace' => 'Professor\\',
    'middleware' => ['auth', 'professor'],

], function() {

    Route::get('recuperacao/{disciplina}/{recuperacao}', ['as' => 'recuperacao.edit', 'uses' => 'RecuperacaoController@edit']);

    Route::group(['prefix' => 'notas', 'as' => 'notas.'], function()
    {
        Route::get('/{disciplina}/edit/{bimestre}', 'NotaController@edit')->name('edit');
        Route::get('/{disciplina}', 'NotaController@show')->name('show');
        Route::post('/', 'NotaController@update')->name('update');

    });

    Route::group(['prefix' => 'disciplinas', 'as' => 'disciplinas'], function() {
        Route::get('/', 'DisciplinaController@index');
        Route::get('/{disciplina}', 'DisciplinaController@show')->name('.show');
        Route::get('/{disciplina}/lista', ['as'=>'.lista', 'uses'=>'DisciplinaController@lista']);
        Route::get('/{disciplina}/boletim', ['as'=>'.boletim', 'uses'=>'NotaController@boletim']);
    });

    Route::group(['prefix' => 'fichaavaliacao', 'as' => 'fichaavaliacao.'], function () {
        Route::get('{disciplina}/{bimestre}/{aluno}', 'DigitarFichaAvaliacaoController@create')->name('create');
        Route::get('{disciplina}/{bimestre}', 'DigitarFichaAvaliacaoController@index')->name('index');
        Route::post('/', 'DigitarFichaAvaliacaoController@store')->name('store');
    });

    Route::resource('solicitacaodigitacao','SolicitacaoDigitacaoController',['except' => ['show']]);
    Route::post('solicitacaodigitacao/show',['as'=>'solicitacaodigitacao.show','uses'=>'SolicitacaoDigitacaoController@show']);

    Route::post('conselhobimestral/view',['as'=>'conselhobimestral.show','uses'=>'ConselhoBimestralController@show']);
    Route::get('conselhobimestral','ConselhoBimestralController@index');

    Route::group(['prefix' => 'perfil','as' => 'perfil'], function()
    {
        Route::get('/', 'PerfilController@index');
        Route::get('edit/dados',['as'=>'.edit.dados', 'uses'=>'PerfilController@index'] );
        Route::post('edit/dados/store',['as'=>'.edit.dados.store', 'uses'=>'PerfilController@update_dados'] );
        Route::post('edit/login/store',['as'=>'.edit.login.store', 'uses'=>'PerfilController@update_login'] );
        Route::get('edit/login', ['as'=>'.edit.login', 'uses'=>'PerfilController@index'] );
        Route::get('config', ['as'=>'.config', 'uses'=>'PerfilController@index']);
    });


});


Route::group([
    'prefix' => 'responsavel',
    'as' => 'responsavel.',
    'namespace' => 'Responsavel\\',
    'middleware' => ['auth', 'responsavel'],
], function() {


});

Route::group([
    'prefix' => 'aluno',
    'as' => 'aluno.',
    'namespace' => 'Aluno\\',
    //'middleware' => ['auth', 'aluno'],
], function() {
    Route::get('/perfil', ['uses' => 'AlunoController@perfil', 'as' => 'perfil']);
});




Route::get('/', 'HomeController@index')->name('home');
Route::get('/home', 'HomeController@index')->name('home');

//Route::post('recuperacao/', ['as' => 'professor.recuperacao.update', 'uses' => 'Professor\RecuperacaoController@update']                         